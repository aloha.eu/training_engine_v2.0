# Training Engine v2.0

Updated version of Training Engine

## How to run?

Locate to: `your_path\to\folder\app`

To build : `docker-compose build`

To run: `docker-compose run`

## How does it work?

### Dataset processing
Training Engine (TE) takes your dataset and parses it into h5py file format. All the pre-processing at this step are minimized in order to allow data reconstruction by the Security Engine.

Depending on task different data formats are supported.

#### Classification task
Required fields for classification task
* `config.data_set_name` - unique dataset name
* `config.data_folder` - full accessible path from docker container 1) in case parsing from folder - path to the folder of the data 2) in case parsing from file - path to the shared data 
* `config.data_file ` - 1) in case parsing from folder - `None` 2) in case parsing from file - full path to the .txt file
* `config.class_names` 1) in case parsing from folder - `[list, of, subfolders, in, your, data_folder, path]` 2) in case parsing from file - `None`
* `config.num_classes ` - number of classes

If you are using parsing from file, be aware that `data_folder` path and pathes listed in your `.txt` file will be concatenated!
Below you can see two examples on how to structure your data. 

Example parsing from folder:
```
config.data_set_name    = "reply_test_3_class"
config.data_folder      = r"/opt/www/shared_data/audio_detection"
config.data_file        = None
config.class_names      = ['down', 'go', 'left']
config.num_classes      = 3
````

Example parsing from file:
```
config.data_set_name    = "reply_test_3_class"
config.data_folder      = r"/opt/www/shared_data/"
config.data_file        = r"/opt/www/shared_data/audio.txt"
config.class_names      = None
config.num_classes      = 3
````
In this case your `audio.txt` should have following information:
```
audio_detection/down/da15e796_nohash_0.wav 0
audio_detection/down/da15e796_nohash_1.wav 0
audio_detection/down/da15e796_nohash_2.wav 0
audio_detection/go/d78858d9_nohash_0.wav 1
audio_detection/go/d78858d9_nohash_1.wav 1
audio_detection/go/d78858d9_nohash_2.wav 1
audio_detection/left/dc6e9c04_nohash_0.wav 2
audio_detection/left/dc6e9c04_nohash_1.wav 2
audio_detection/left/dc6e9c04_nohash_2.wav 2
audio_detection/left/dc6e9c04_nohash_3.wav 2
```

#### Segmentation task
Required fields for classification task
    - from folder
    - from mat file
#### Detection task
Required fields for classification task
    - from json file - ImageNet format
    

Processing diagram:
- loads information about dataset from config
- checks availability of the data and information provided
- produces following prepossessing:
    - computes actual amount of labels, in case of wrong number - updates corresponding value in `ConfigurationFile` entry
    - for audio classification case adds silence as additional class in case `silence_pcnt != 0`
    - for detection case transforms bounding boxes in yolo format
    - for all images transforms images to given image size
- computes mean and variance for all dataset
- splits data into training and validation, indices of training and validation data are stored in `slice_split`

### Training

Training is possible to perform either on predefined models: `'ConvNet', 'VGG19', 'UNet', 'YoloV3Tiny'` or custom onnx model.
In case of custom model, the model should be located in `shared_data/onnx_models` folder and it's name should be specified
in the field `onnx` of the `AlgorithmConfiguration`.

During training we keep the track of the best network based on validation loss. By the end of the training the best model is stored.
All information of losses and accuracies are stored using pytorch package `SummaryWriter`. Results of it can be embedded directly to tensorboard.

## MongoDB instances description

### ConfigurationFile
* global variable:
    * `project`  - mandatory - by this value we check existence of the config file

* dataset configuration:
    * `data_set_name = ['MNIST', 'CIFAR10', 'CIFAR100', 'custom_name']`  - mandatory
    * `data_folder  = [None, 'path\to\folder']    ` - mandatory
    * `data_file    = [None, 'path\to\file']      ` - mandatory
    * `class_names = [None, [list, of, classes]] ` - mandatory
    * `num_classes ` - mandatory
    * `data_split = [0.1 ... 0.9]` - mandatory
    * `cross_val = 1 ` - mandatory/default
    * `image_dimensions - [h, w, c]` - mandatory

* learning configuration:
    * `task_type = ['classification', 'segmentation', 'detection']` - mandatory
    * `num_epochs ` - mandatory
    * `lr ` - mandatory
    * `lr_decay ` - mandatory
    * `ref_steps ` - mandatory
    * `ref_patience` - mandatory
    * `batch_size ` - mandatory
    * `loss = ['percent', 'dice', 'detection_loss'] ` - mandatory
    * `accuracy = ['percent', 'IoU', 'mAP']` - mandatory
    * `optimizer = ['adam', 'sgd']` - mandatory
    * `nonlin = ['relu', 'lrelu']` - used only if library networks is chosen
    * `num_filters ` - used only if library networks is chosen

* transformations:
    * `training_mode = 'full'` - mandatory
    * ```augment_dict = {'HorizontalFlip': [None, [0.1 ... 0.9]], 'VerticalFlip': [None, [0.1 ... 0.9]], 'Scale': [None, [0.1 ... 0.9]], 'Rotate': [None, [0.1 ... 0.9]], 'Normalize': [False, True], 'AudioAugmentation': [False, True], 'AddBGNoise': [False, True]} ``` - mandatory

* yolo parameters:
    * `anchors = [[81, 82, 135, 169, 344, 319], [23, 27, 37, 58, 81, 82]]` - mandatory / default
    * `divider = [32, 16]` - mandatory / default
    * `giou_loss_gain = 3.54` - mandatory / default
    * `cls_loss_gain = 37.4` - mandatory / default
    * `obj_loss_gain = 64.3` - mandatory / default
    * `iou_t = 0.20` - mandatory / default
    * `iou_thres = 0.5` - mandatory / default
    * `conf_thres = 0.5` - mandatory / default

* audio parameters:
    * `silence_pcnt = [0.0 ... 0.9]` - mandatory - silence added as separate class with given % proportion to dataset
    * `bg_noise_pcnt = [0.0 ... 0.9]` - mandatory - to this % of dataset will be added random background noise
    * `sample_rate = 16000` - mandatory / default
    * `n_mels = 32` - mandatory / default


### Dataset
* global variables:
    * `project`  - mandatory - by this value we check existence of the dataset
    * `name`  - mandatory - name of the dataset

* parameters filled up after dataset parsing:
    * `path`  - mandatory - path to a h5py file
    * `num_classes`  - mandatory - actual number of classes in a dataset
    * `mean`  - mandatory - mean value of a dataset
    * `var`  - mandatory - variance value of a dataset
    * `slice_split`  - mandatory - list of indexes specifying training and validation data


### AlgorithmConfiguration
* global variables:
    * `project`  - mandatory - by this value we check existence of the design point
    * `onnx = ['ConvNet', 'VGG19', 'UNet', 'YoloV3Tiny', 'your_model.onnx']`  - mandatory  

* parameters filled up after training:
    * `onnx_trained`  - mandatory - path to trained model in onnx format
    * `pytorch_trained `  - mandatory - path to trained model in onnx format
    * `training_accuracy`  - mandatory - training accuracy of trained model
    * `training_loss`  - mandatory - training loss of trained model
    * `validation_accuracy`  - mandatory - validation accuracy of trained model
    * `validation_loss `  - mandatory - validation loss of trained model
    * `training_log`  - mandatory - path to training logs stored in tensorboard format
    * `execution_time`  - mandatory - overall time spend on training and validation of the model


## Small examples
You can find two examples:
- `mnist_example.ipynb` - classification on mnist
- `audio_classification_example.ipynb` - classification with custom onnx file for audio classification on reduced
(due to space issues) speech detection dataset


## For modifications
Please use fork instrument in order to copy master brunch and do modification only in your own brunch:
https://www.tutorialspoint.com/gitlab/gitlab_create_branch.htm

## For bugs/requests/etc.
For all the bugs, issues, requests, and other information please use Bug Report Template:
https://github.com/qTox/qTox/wiki/Writing-Useful-Bug-Reports

##### All issues that will not follow this template will be ignored

This readme file as well as running examples will be updated *naturally* and *upon request*
