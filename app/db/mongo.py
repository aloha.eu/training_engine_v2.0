## Generate MongoDB models from YAML definitions
### Example: https://stackoverflow.com/questions/6311738/create-a-model-from-yaml-json-on-the-fly

from flask_mongoengine import MongoEngine
from mongoengine.connection import get_db, connect

class MongoConnector():

    def __init__(self, app):
        self.mongo_engine = MongoEngine()
        self.mongo_engine.init_app(app)

    # Obtain db connection(s)
        self.db = get_db()
        #print("Database stats: ", self.db.command('dbstats'))