FROM nvidia/cuda:10.2-runtime-ubuntu16.04
ENV DEBIAN_FRONTEND=noninteractive

ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES}
	  
ENV BUILD_PACKAGES="\
        build-essential \
        python3-dev \
        cmake \
        tcl-dev \
        xz-utils \
        zlib1g-dev \
        git \
        curl" \
    APT_PACKAGES="\
        ca-certificates \
        openssl \
        bash \
        graphviz \
        fonts-noto \
        libpng16-16 \
        libfreetype6 \
		redis-server \
        python3 \
        python3-pip \
 		python3-setuptools \
		libopencv-dev \
 		python-flask \
		python-opencv \
		libsndfile1 \
		ffmpeg" \
    PYTHON_VERSION=3.6.4 \
    PATH=/usr/local/bin:$PATH \
    PYTHON_PIP_VERSION=9.0.1 \
    JUPYTER_CONFIG_DIR=/home/.ipython/profile_default/startup \
    LANG=C.UTF-8

COPY requirements.txt ./
	
RUN set -ex; \
    apt-get update -y; \
    apt-get install -y --no-install-recommends ${APT_PACKAGES}; \
    apt-get install -y --no-install-recommends ${BUILD_PACKAGES}; \
    pip3 install --upgrade pip; \
    hash -r; \
    pip3 install -U -v setuptools wheel; \
    pip3 install -r requirements.txt; \
    apt-get remove --purge --auto-remove -y ${BUILD_PACKAGES}; \
    apt-get clean; \
    apt-get autoclean; \
    apt-get autoremove; \
    rm -rf /tmp/* /var/tmp/*; \
    rm -rf /var/lib/apt/lists/*; \
    rm -f /var/cache/apt/archives/*.deb \
        /var/cache/apt/archives/partial/*.deb \
        /var/cache/apt/*.bin; \
    find /usr/lib/python3 -name __pycache__ | xargs rm -r; \
    rm -rf /root/.[acpw]*; \
    pip3 install jupyter && jupyter nbextension enable --py widgetsnbextension; \
    mkdir -p ${JUPYTER_CONFIG_DIR}; \
    echo "import warnings" | tee ${JUPYTER_CONFIG_DIR}/config.py; \
    echo "warnings.filterwarnings('ignore')" | tee -a ${JUPYTER_CONFIG_DIR}/config.py; \
	echo "c.NotebookApp.token = u''" | tee -a ${JUPYTER_CONFIG_DIR}/config.py
	ENV PYTHONPATH "${PYTHONPATH}:/opt/www/api/engine"
	ENV PYTHONPATH "${PYTHONPATH}:/opt/www/shared_data"
	ENV PYTHONPATH "${PYTHONPATH}:/opt/www"
	ENV PYTHONPATH "${PYTHONPATH}:/opt/www/api/engine/utils/onnxparser"
	ENV PATH "$PATH:/opt/www"

EXPOSE 8888
EXPOSE 6379
EXPOSE 5000

ENV API_SERVER_HOME=/opt/www
WORKDIR "$API_SERVER_HOME"
COPY "./" "./"

CMD [ "jupyter", "notebook", "--port=8888", "--no-browser", "--allow-root", "--ip=0.0.0.0", "--NotebookApp.token=", "python3", "__init.py__"]
