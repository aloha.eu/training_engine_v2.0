# -*- coding: utf-8 -*-

###
### DO NOT CHANGE THIS FILE
### 
### The code is auto generated, your change will be overwritten by 
### code generating.
###
from __future__ import absolute_import

from .api.models import Models
from .api.models_modelId import ModelsModelid
from .api.training_call import TrainingCall
from .api.training_respond import TrainingRespond


routes = [
    dict(resource=Models, urls=['/models'], endpoint='models'),
    dict(resource=ModelsModelid, urls=['/models/<modelId>'], endpoint='models_modelId'),
    dict(resource=TrainingCall, urls=['/training_call'], endpoint='training_call'),
    dict(resource=TrainingRespond, urls=['/training_respond'], endpoint='training_respond'),
]