#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 3/22/2020 10:23 PM $
# by : clu $
# SVN : $
#


import os
import cv2
import librosa
import numpy as np
import billiard as mp
import matplotlib.pyplot as plt
from hurry.filesize import size
import itertools
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score



def load_image(img_size, im_file, l='img'):
    if l == 'msk':
        img = cv2.imread(im_file, cv2.IMREAD_GRAYSCALE)
    else:
      if img_size[2]==1:
        img = cv2.imread(im_file, cv2.IMREAD_GRAYSCALE)
      else:
        img = cv2.imread(im_file)
    if len(img_size) == 2:
        img = img[:, :, 0]
    if img.max() > 1 and l != 'msk':
        img = img/255
    if img.shape[0] != img_size[0] and img.shape[1] != img_size[1]:
        img = cv2.resize(img, (img_size[1], img_size[0]), interpolation=cv2.INTER_LINEAR)
    if len(img.shape) == 2:
        img = np.expand_dims(img, -1)
    return img



def load_audio(sample_rate, im_file):
    if im_file:
        samples, _ = librosa.load(im_file, sample_rate)
        length = int(1 * sample_rate)
        if length < len(samples):
            samples = samples[:length]
        elif length > len(samples):
            samples = np.pad(samples, (0, length - len(samples)), "constant")
    else:
        # silence
        samples = np.zeros(sample_rate, dtype=np.float32)
    return samples

def scale_minmax(X, min=0.0, max=1.0):
    X_std = (X - X.min()) / (X.max() - X.min())
    X_scaled = X_std * (max - min) + min
    return X_scaled

def one_hot_encode(labels):
    if not isinstance(labels[0], list):
        mx = max(labels)
        y = []
        for i in range(len(labels)):
            f = [0] * (mx + 1)
            f[labels[i]] = 1
            y.append(f)
        return y
    else:
        return labels

def bulk_process_audio(audio_proc, in_file):
    samples = load_audio(audio_proc[0], in_file)

    s = librosa.feature.melspectrogram(samples, sr=audio_proc[0], n_mels=audio_proc[1])
    mel_spec = librosa.power_to_db(s, ref=np.max)
    mean = np.mean(mel_spec, axis=(0, 1))
    var = np.var(mel_spec, axis=(0, 1))
    del s, mel_spec

    return [samples, mean, var]


def bulk_process_image(im_inp, in_file):
    img_size = im_inp[0]
    l = im_inp[1]
    im = load_image(img_size, in_file, l)
    return [im, np.mean(im, axis=(0, 1)), np.var(im, axis=(0, 1))]


def bulk_process_detection(img_size, in_file):
    img = cv2.imread(in_file)
    if len(img_size) == 2:
        img = img[:, :, 0]
    if img.max() > 1:
        img = img/255
    img, pad = pad_to_square(img)
    if img.shape[0] != img_size[0] and img.shape[1] != img_size[1]:
        img = cv2.resize(img, (img_size[1], img_size[0]), interpolation=cv2.INTER_LINEAR)

    return [img, np.mean(img, axis=(0, 1)), np.var(img, axis=(0, 1)), pad, img.shape]



def pad_to_square(img):
    h, w, _ = img.shape
    dim_diff = np.abs(h - w)
    # (upper / left) padding and (lower / right) padding
    pad1, pad2 = dim_diff // 2, dim_diff - dim_diff // 2
    # Determine padding
    pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)
    # Add padding
    img = cv2.copyMakeBorder(img, pad[2], pad[3], pad[0], pad[1], cv2.BORDER_CONSTANT, value=0)

    return img, pad


def count_size(file):
    use_compressed_size = False
    
    if use_compressed_size:
      if os.path.exists(file):
        return os.stat(file).st_size
      else:
        return 0
    else:
      if os.path.exists(file):
        img=cv2.imread(file)
        return np.prod(img.shape)*1 # uint8
      else:
        return 0


def to_one_hot(labels):
    """
    Convert labels to one-hot
    :param labels:
    :return:
    """
    mx = max(labels)
    y = []
    for i in range(len(labels)):
        f = [0] * (mx + 1)
        f[labels[i]] = 1
        y.append(f)
    return y


def chunk_split(data_list, splits):
    avg = len(data_list) / float(splits)
    out = []
    last = 0.0
    while last < len(data_list):
        out.append(data_list[int(last):int(last + avg)])
        last += avg
    return out


def path_walk(path):
    path_list = []
    for dirpath, _, filenames in os.walk(path):
        for f in filenames:
            if f.lower().endswith(tuple(['.jpg', '.jpeg', '.png'])):
                path_list.append(os.path.abspath(os.path.join(dirpath, f)))
    return path_list

def one_to_onehot(label, max_label):
    y = [0 for i in range(max_label)]
    y[label] = 1
    return y

def prepare_memory_splits(free_mem, x_list):
    print("start data dump")
    pool = mp.Pool(mp.cpu_count())
    data_mem_x = sum(pool.map(count_size, x_list))
    pool.close()
    pool.join()
    # if free_mem > data_mem_x:
    #     splits = 1
    # else:
    #     splits = (free_mem // data_mem_x) + 1
    # splits = (free_mem // data_mem_x) + 1

    print("Full uncompressed dataset size: {}".format(size(data_mem_x)))

    splits = int((data_mem_x / free_mem) + 1)
    return chunk_split(x_list, splits), splits
