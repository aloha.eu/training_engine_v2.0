#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 02/21/2018 16:55 $
# by : shepeleva $
# SVN : $
#

# --- imports -----------------------------------------------------------------

from utils.transforms import *
from torchvision import transforms

# used to load, configure and init transforms objects
from plugins.utils.pipeline import Pipeline, Compose

import numpy as np
import sys
class BatchIterator:
    def __init__(self, file_name, data_split, img_size, num_class, batch_size, task_type, device, trainval_key, mean, var,
                 preprocessing_pipeline_config=None, augment_dict=None, shuffle_key=False):
        self.file_name = file_name
        self.preprocessing_pipeline_instance = Pipeline(preprocessing_pipeline_config) #TODO add check for None?
        self.data_split = data_split
        self.task_type = task_type
        self.augment_dict = augment_dict
        self.device = device
        self.trainval_key = trainval_key
        self.mean = mean
        self.var = var
        self._preprocess_data(img_size, num_class, batch_size)
        if shuffle_key:
            self._shuffle()
        else:
            self.permutation_list = self.data_split
        if (self.task_type == 'classification') or (self.task_type == 'segmentation') or (self.task_type == 'detection'):
            it = [self.permutation_list[i:i+self.batch_size] for i in range(0, len(self.permutation_list), self.batch_size)]
            self.iterator = iter(it)
        self.current = next(self.iterator)

    def _preprocess_data(self, img_size, num_class, batch_size):
        self.X_key = 'X_data'
        self.y_key = 'y_data'
        self.size = len(self.data_split)
        self.img_size = img_size
        self.num_class = num_class
        self.batch_size = batch_size
        self.num_minibatch = int(self.size / self.batch_size) + 1
        self.count = 0
        self.max_lim = self.size
        self.transforms = self._compose_transforms()

    def get_max_lim(self):
        return int(self.max_lim / self.batch_size) + 1

    def _shuffle(self):
        import random
        self.permutation_list = random.sample(self.data_split, len(self.data_split))

    def __iter__(self):
        if self.task_type == 'classification':
            while self.count < self.num_minibatch:
                try:
                    yield self._next_batch_classification()
                    self.count = self.count + 1
                    self.current = next(self.iterator)
                except StopIteration:
                    break
        if self.task_type == 'segmentation':
            while self.count < self.num_minibatch:
                try:
                    yield self._next_batch_segmentation()
                    self.count = self.count + 1
                    self.current = next(self.iterator)
                except StopIteration:
                    break
        if self.task_type == 'detection':
            while self.count < self.num_minibatch:
                try:
                    yield self._next_batch_detection()
                    self.count = self.count + 1
                    self.current = next(self.iterator)
                except StopIteration:
                    break

    def _next_batch_classification(self):
        for i in self.current:
            imgs = self.file_name[self.X_key][i]
            lbls = self.file_name[self.y_key][i]
            try:
                #yield self.transforms([imgs, lbls])
                yield [self.transforms[0](imgs), lbls]
                # self.count = self.count + 1
                # self.current = next(self.iterator)
            except StopIteration:
                break
        # self.count = self.count + 1
        # self.current = next(self.iterator)

    def _next_batch_segmentation(self):
        for i in self.current:
            imgs = self.file_name[self.X_key][i]
            msks = self.file_name[self.y_key][i]
            msks = np.expand_dims(msks, axis=2)
            # if self.num_class == 1:
            #     msks = np.expand_dims(msks, 0)
            # elif self.num_class == 2:
            #     msks = np.array([msks, np.ones(msks.shape) - msks], dtype="uint8").squeeze(-1)
            if self.num_class >= 2:
                msks = self._recode_masks(msks)
            # elif self.num_class < 2:
            #     raise ValueError("You should have 2 or more classes to perform segmentation")
            try:
                
                seed = random.randrange(sys.maxsize)
                
                yield [self.transforms[0](imgs, seed=seed), self.transforms[1](msks, seed=seed)]
                # self.count = self.count + 1
                # self.current = next(self.iterator)
            except StopIteration:
                break
        # self.count = self.count + 1
        # self.current = next(self.iterator)

    def _next_batch_detection(self):
        im_id = 0
        for i in self.current:
            lbls = []
            imgs = self.file_name[self.X_key][i]
            lbls_ = self.file_name[self.y_key][i]
          #  print ("lbls_")
           # print (lbls_)
            lbls_ = np.delete(lbls_, np.where(lbls_.sum(axis=1) == 0), axis=0).tolist()
           # print ("lbls_")
           # print (lbls_)
#            for lst in lbls_:
#                lbls.append([im_id] + lst)
#            im_id += 1
#            print ("lbls")
#            print (lbls)

            try:
                yield [self.transforms[0](imgs), lbls_]
                # yield self.transforms([imgs, lbls]), imgs.shape
                # self.count = self.count + 1
                # self.current = next(self.iterator)
            except StopIteration:
                break
        # self.count = self.count + 1
        # self.current = next(self.iterator)

    def _recode_masks(self, msks):
        new_img = np.zeros([msks.shape[0], msks.shape[1], self.num_class])
        # img = msks[:, :, 0]
        for j in range(1, self.num_class+1):
            x_ind, y_ind = np.where(msks == j)
            for ind in range(0, len(x_ind)):
                new_img[x_ind[[ind]], y_ind[ind], j] = 1
        return new_img

    def _compose_transforms(self):
        batch_transforms     = []
        batch_transforms_lbl = []

        if self.trainval_key == 'train':
            # Load plugins to perform batch transforms, train specific #TODO train specific
            print("[Preprocessing pipeline] Load sample transforms")
            batch_transforms     = self.preprocessing_pipeline_instance.get_transforms('batch_transforms')
            try:
              batch_transforms_lbl = self.preprocessing_pipeline_instance.get_transforms('batch_transforms_lbl')
            except:
              pass
        elif self.trainval_key == 'valid':
            # Load plugins to perform batch transforms, validation specific #TODO validation specific
            print("[Preprocessing pipeline] Load sample transforms, only validation")
            batch_transforms     = self.preprocessing_pipeline_instance.get_transforms('batch_transforms', validation=True)
            try:
              batch_transforms_lbl = self.preprocessing_pipeline_instance.get_transforms('batch_transforms_lbl', validation=True)
            except:
              pass
        else:
            raise ValueError('Training or validation key is allowed')
        
        if len(batch_transforms)<1:
            print("Error! At least one batch transform must be specified currently, this may change in the future")
            raise ValueError

        return [Compose(batch_transforms), Compose(batch_transforms_lbl)]
        
        
