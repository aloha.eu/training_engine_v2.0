import numpy as np
import torch
import math
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

#VOC order
#{"airplane", "bicycle", "bird", "boat", "bottle", "bus", "car",
#"cat", "chair", "cow", "dining table", "dog", "horse", "motorcycle", "person", "potted plant", "sheep", "couch", "train", "tv"}

#COCO order
#{"person","bicycle","car","motorcycle","airplane","bus","train","boat","bird","cat","dog","horse",
#"sheep","cow","bottle","chair","couch","potted plant","dining table","tv"}

lcolors = [[255, 0,  128],
           [0,   255, 255],
           [128, 128, 0],
           [0,   128, 128],
           [128, 0,   255],
           [0,   0,   0],
           [0,   255, 128],
           [0,   255, 0],
           [128, 255, 255],
           [0,   0,   128],
           [128, 0,   0],
           [128, 0,   128],
           [0,   128, 0],
           [128, 255, 0],
           [128, 128, 255],
           [0,   128, 255],
           [128, 128, 128],
           [0,   0,   255],
           [128, 255, 128],
           [255, 0,   0]
           ]
 
lcolors = [[x / 255 for x in c] for c in lcolors]

def draw_bb( image, bbi, img_size):
    
    bb = [max(0, min(img_size[0] - 1, bbi[0])),
          max(0, min(img_size[1] - 1, bbi[1])),
          min(img_size[0] - 1, max(0, bbi[2])),
          min(img_size[1] - 1, max(0, bbi[3]))]
          
    bb = [int(i) for i in bb]
    #print("image",image.shape)
    #print("image bb",bb)
    #classl = 0
    classl = int(bbi[5])
    if bb[0]<bb[2] and bb[1]<bb[3]:
      for y in range(bb[1],bb[3]):
          image[0][y][bb[0]] = lcolors[classl][0]
          image[1][y][bb[0]] = lcolors[classl][1]
          image[2][y][bb[0]] = lcolors[classl][2]
          image[0][y][bb[2]] = lcolors[classl][0]
          image[1][y][bb[2]] = lcolors[classl][1]
          image[2][y][bb[2]] = lcolors[classl][2]

      for x in range(bb[0],bb[2]):
          image[0][bb[1]][x] = lcolors[classl][0]
          image[1][bb[1]][x] = lcolors[classl][1]
          image[2][bb[1]][x] = lcolors[classl][2]
          image[0][bb[3]][x] = lcolors[classl][0]
          image[1][bb[3]][x] = lcolors[classl][1]
          image[2][bb[3]][x] = lcolors[classl][2]


def draw_bb_list( image, bbi_list,img_size):
    for bb in bbi_list[:20]:
        draw_bb(image,bb,img_size)


def post_processing(logits, image_size=416, conf_threshold=0.5, nms_threshold=0.5,
                    anchors=[(1.3221, 1.73145),
                             (3.19275, 4.00944),
                             (5.05587, 8.09892),
                             (9.47112, 4.84053),
                             (11.2364, 10.0071)]):
    num_anchors = len(anchors)
    anchors = torch.Tensor(anchors)
    if isinstance(logits, Variable):
        logits = logits.data

    if logits.dim() == 3:
        logits.unsqueeze_(0)

    batch = logits.size(0)
    w = logits.size(2)
    h = logits.size(3)

    # Compute xc,yc, w,h, box_score on Tensor
    lin_x = torch.linspace(0, w - 1, w).repeat(h, 1).view(h * w)
    lin_y = torch.linspace(0, h - 1, h).repeat(w, 1).t().contiguous().view(h * w)
    anchor_w = anchors[:, 0].contiguous().view(1, num_anchors, 1)
    anchor_h = anchors[:, 1].contiguous().view(1, num_anchors, 1)
    if torch.cuda.is_available():
        lin_x = lin_x.cuda()
        lin_y = lin_y.cuda()
        anchor_w = anchor_w.cuda()
        anchor_h = anchor_h.cuda()

    logits = logits.view(batch, num_anchors, -1, h * w)
    logits[:, :, 0, :].sigmoid_().add_(lin_x).div_(w) #xc
    logits[:, :, 1, :].sigmoid_().add_(lin_y).div_(h) #yc
    logits[:, :, 2, :].exp_().mul_(anchor_w).div_(w)  #w
    logits[:, :, 3, :].exp_().mul_(anchor_h).div_(h)  #h
    logits[:, :, 4, :].sigmoid_()                     #box score

    # print("  post_processing conf",logits[:, :, 4, :].max(),logits[:, :, 4, :].sum()/logits[:, :, 4, :].numel())

    with torch.no_grad():
        cls_scores = torch.nn.functional.softmax(logits[:, :, 5:, :], 2)
    cls_max, cls_max_idx = torch.max(cls_scores, 2)
    cls_max_idx = cls_max_idx.float()
    cls_max.mul_(logits[:, :, 4, :])

    score_thresh = cls_max > conf_threshold
    score_thresh_flat = score_thresh.view(-1)

    if score_thresh.sum() == 0:
        predicted_boxes = []
        for i in range(batch):
            predicted_boxes.append(torch.Tensor([]))
    else:
        coords = logits.transpose(2, 3)[..., 0:4]
        coords = coords[score_thresh[..., None].expand_as(coords)].view(-1, 4)
        scores = cls_max[score_thresh]
        idx = cls_max_idx[score_thresh]
        detections = torch.cat([coords, scores[:, None], idx[:, None]], dim=1)

        max_det_per_batch = num_anchors * h * w
        slices = [slice(max_det_per_batch * i, max_det_per_batch * (i + 1)) for i in range(batch)]
        det_per_batch = torch.IntTensor([score_thresh_flat[s].int().sum() for s in slices])
        split_idx = torch.cumsum(det_per_batch, dim=0)

        # Group detections per image of batch
        predicted_boxes = []
        start = 0
        for end in split_idx:
            predicted_boxes.append(detections[start: end])
            start = end

    selected_boxes = []
    for boxes in predicted_boxes:
        if boxes.numel() == 0:
            return boxes

        a = boxes[:, :2]
        b = boxes[:, 2:4]
        bboxes_cpy = torch.cat([a - b / 2, a + b / 2], 1)
        scores = boxes[:, 4]

        # Sort coordinates by descending score
        scores, order = scores.sort(0, descending=True)
        x1, y1, x2, y2 = bboxes_cpy[order].split(1, 1)

        # Compute dx and dy between each pair of boxes (these mat contain every pair twice...)
        dx = (x2.min(x2.t()) - x1.max(x1.t())).clamp(min=0)
        dy = (y2.min(y2.t()) - y1.max(y1.t())).clamp(min=0)

        # Compute iou
        intersections = dx * dy
        areas = (x2 - x1) * (y2 - y1)
        unions = (areas + areas.t()) - intersections
        ious = intersections / unions

        ious[ious < nms_threshold] = 0
        # Filter based on iou (and class)
        conflicting = ious.triu(1)

        keep = conflicting.sum(0)
        keep = keep.cpu()
        conflicting = conflicting.cpu()

        keep_len = len(keep) - 1
        for i in range(1, keep_len):
            if keep[i] > 0:
                keep -= conflicting[i]
        if torch.cuda.is_available():
            keep = keep.cuda()

        keep = (keep == 0)
        selected_boxes.append(boxes[order][keep[:, None].expand_as(boxes)].view(-1, 6).contiguous())

    final_boxes = []
    for boxes in selected_boxes:
        if boxes.dim() == 0:
            final_boxes.append([])
        else:
            boxes[:, 0:3:2] *= image_size
            boxes[:, 0]     -= boxes[:, 2] / 2
            boxes[:, 2]      = boxes[:, 0] + boxes[:, 2]
            boxes[:, 1:4:2] *= image_size
            boxes[:, 1]     -= boxes[:, 3] / 2
            boxes[:, 3]      = boxes[:, 1] + boxes[:, 3]

            final_boxes.append(torch.tensor([[box[0].item(),
                                              box[1].item(),
                                              box[2].item(),
                                              box[3].item(),
                                              box[4].item(),
                                          int(box[5].item())] for box in boxes]))
    return final_boxes


class Image_logger:
    def __init__(self):
        self.done_for_epoch = False;


    def clear(self):
        self.done_for_epoch = False
        

        
    def draw_results(self,text, predictions, tinput, img_size, tbx_writer,epoch) :   
        
        if (not self.done_for_epoch) and (len(predictions)) > 0:
        #    print("######### predictions ", text +  str(len(predictions)))
            rand_idx = np.arange(len(predictions))
            np.random.shuffle(rand_idx)
            for idx in np.nditer(rand_idx[:2]):
                pred = predictions[idx]
                if len(pred):
                    timage = tinput[idx,:,:,:].clone()
                    draw_bb_list(timage, pred, img_size)
               #     tbx_writer.add_image(text + str(idx),timage[torch.LongTensor([2,1,0])],epoch)
                    tbx_writer.add_image(text + str(idx),timage,epoch)
                    self.done_for_epoch = True
    
def draw_results_(text, predictions, tinput, img_size, tbx_writer, epoch):
    #print("######### predictions ", text +  str(len(predictions)))
    if len(predictions) > 0:
        rand_idx = np.arange(len(predictions))
        np.random.shuffle(rand_idx)
        #print("rand_idx",rand_idx[:4])
        for idx in np.nditer(rand_idx[:4]):
            pred = predictions[idx]
            if len(pred):
                timage = tinput[idx,:,:,:].clone()
                draw_bb_list(timage,pred,img_size)
            #    tbx_writer.add_image(text + str(idx), 256 - timage[torch.LongTensor([2,1,0])],epoch)
                tbx_writer.add_image(text + str(idx), timage,epoch)
                #print("  ",pred[:10])
        #for idx, pred in enumerate(predictions):
            #if len(pred):
                #timage = tinput[idx,:,:,:].clone()
                #draw_bb_list(timage,pred,img_size)
                #tbx_writer.add_image(text + str(idx), 256 - timage[torch.LongTensor([2,1,0])],epoch)
                #print("  ",pred[:10])

from tqdm import tqdm

def get_batch_statistics(outputs, targets, iou_threshold):
    """ Compute true positives, predicted scores and predicted labels per sample """
    #targets = np.asarray(orig_targets)
    #targets = np.reshape(targets,(targets.shape[0]*targets.shape[1],targets.shape[2]))
    #targets = np.insert(targets,0,np.arange(targets.shape[0]),axis=1)
    #print("get_batch_statistics outputs '\n'",outputs)
    #print("get_batch_statistics targets '\n'",targets)
    
    batch_metrics = []
    #print("len(outputs)",len(outputs))
    for sample_i in range(len(outputs)):
        #print("sample_i",sample_i)
        if outputs[sample_i] is None:
            continue

        output = outputs[sample_i]
        pred_boxes = output[:, :4]
        pred_scores = output[:, 4]
        pred_labels = output[:, -1]

        true_positives = np.zeros(pred_boxes.shape[0])
        #print("targets",targets)
        #print("targets[:, 0]",targets[:, 0])
        annotations = targets[targets[:, 0] == sample_i][:, 1:]
        target_labels = annotations[:, 0] if len(annotations) else []
        #print("target_labels",target_labels)
        #print("len(annotations)",len(annotations))
        #print("annotations",annotations)
        if len(annotations):
            detected_boxes = []
            target_boxes = annotations[:, 1:]

            for pred_i, (pred_box, pred_label) in enumerate(zip(pred_boxes, pred_labels)):

                # If targets are found break
                if len(detected_boxes) == len(annotations):
                    break

                # Ignore if label is not one of the target labels
                #print("pred_label",pred_label,type(pred_label))
                #print("target_labels",target_labels,type(target_labels))
                if pred_label not in target_labels:
                    continue
                #print("#########pred_box",pred_box)
                #print("#########target_boxes",target_boxes)
                
                iou, box_index = bbox_iou(pred_box.unsqueeze(0), target_boxes).max(0)
                #print("iou",iou)
                if iou >= iou_threshold and box_index not in detected_boxes:
                    true_positives[pred_i] = 1
                    detected_boxes += [box_index]
                    target_boxes[box_index] = torch.tensor([0,0,0,0])
        batch_metrics.append([true_positives, pred_scores, pred_labels])
    return batch_metrics

def ap_per_class(tp, conf, pred_cls, target_cls):
    """ Compute the average precision, given the recall and precision curves.
    Source: https://github.com/rafaelpadilla/Object-Detection-Metrics.
    # Arguments
        tp:    True positives (list).
        conf:  Objectness value from 0-1 (list).
        pred_cls: Predicted object classes (list).
        target_cls: True object classes (list).
    # Returns
        The average precision as computed in py-faster-rcnn.
    """

    # Sort by objectness
    i = np.argsort(-conf)
    tp, conf, pred_cls = tp[i], conf[i], pred_cls[i]
    #print("tp",tp,type(tp))
    # Find unique classes
    unique_classes = np.unique(target_cls)

    # Create Precision-Recall curve and compute AP for each class
    ap, p, r = [], [], []
    for c in tqdm(unique_classes, desc="Computing AP"):
        i = pred_cls == c
        n_gt = (target_cls == c).sum()  # Number of ground truth objects
        n_p = i.sum()  # Number of predicted objects

        if n_p == 0 and n_gt == 0:
            continue
        elif n_p == 0 or n_gt == 0:
            ap.append(0)
            r.append(0)
            p.append(0)
        else:
            # Accumulate FPs and TPs
            fpc = (1 - tp[i]).cumsum()
            tpc = (tp[i]).cumsum()

            # Recall
            #print("tpc",tpc,type(tpc))
            #print("n_gt",n_gt,type(n_gt))
            
            recall_curve = tpc / (n_gt + 1e-16)
            r.append(recall_curve[-1])

            # Precision
            precision_curve = tpc / (tpc + fpc)
            p.append(precision_curve[-1])

            # AP from recall-precision curve
            ap.append(compute_ap(recall_curve, precision_curve))

    # Compute F1 score (harmonic mean of precision and recall)
    p, r, ap = np.array(p), np.array(r), np.array(ap)
    f1 = 2 * p * r / (p + r + 1e-16)

    return p, r, ap, f1, unique_classes.astype("int32")

def bbox_iou(box1, box2, x1y1x2y2=True):
    """
    Returns the IoU of two bounding boxes
    """
    if not x1y1x2y2:
        # Transform from center and width to exact coordinates
        b1_x1, b1_x2 = box1[:, 0] - box1[:, 2] / 2, box1[:, 0] + box1[:, 2] / 2
        b1_y1, b1_y2 = box1[:, 1] - box1[:, 3] / 2, box1[:, 1] + box1[:, 3] / 2
        b2_x1, b2_x2 = box2[:, 0] - box2[:, 2] / 2, box2[:, 0] + box2[:, 2] / 2
        b2_y1, b2_y2 = box2[:, 1] - box2[:, 3] / 2, box2[:, 1] + box2[:, 3] / 2
    else:
        # Get the coordinates of bounding boxes
        b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
        b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]

    # get the corrdinates of the intersection rectangle
    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)
    # Intersection area
    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1 + 1, min=0
    )
    # Union Area
    b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)

    return iou

def compute_ap(recall, precision):
    """ Compute the average precision, given the recall and precision curves.
    Code originally from https://github.com/rbgirshick/py-faster-rcnn.
    # Arguments
        recall:    The recall curve (list).
        precision: The precision curve (list).
    # Returns
        The average precision as computed in py-faster-rcnn.
    """
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.0], recall, [1.0]))
    mpre = np.concatenate(([0.0], precision, [0.0]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap


def calc_mAP(mAP_acc_list,labels):
        
        true_positives = np.asarray([])
        pred_scores = np.asarray([])
        pred_labels = np.asarray([])
        pplist =  list(zip(*mAP_acc_list))
        if len(pplist) > 0:
            true_positives, pred_scores, pred_labels = [np.concatenate(x, 0) for x in pplist]        
        #print("true positive", true_positives, type(true_positives))
        #print("true pred_scores", pred_scores, type(pred_scores))
        #print("true pred_labels", pred_labels, type(pred_labels))
        #print("true labels", labels, type(labels))

        precision, recall, AP, f1, ap_class = ap_per_class(true_positives, pred_scores, pred_labels, labels)
        #print("precision",precision)
        #print("recall",recall)
        #print("AP",AP)
        #print("f1",f1)
        if AP.size > 0:
            return AP.sum()/AP.size
        
        return 0
    
    
class mAP_Calculator:
    def __init__(self):
        self.mAP_acc_list = []
        self.mAP_acc_lables = []
        self.iou_threshold = 0.4
        
    def accumulate(self, predictions, ff ):
        tmp_targets = [f[1] for f in ff]
        acc = 0
        for ar in tmp_targets:
            acc += ar.shape[0]
            
        targets = torch.empty([acc, 6])

        tensor_idx = 0
        for idx,ar in enumerate(tmp_targets):
            for idx_ar,e in enumerate(ar):                                
                targets[tensor_idx,0] = idx
                #targets[tensor_idx,1] = np.asscalar(np.argmax(e[4:])) for one hot format
                targets[tensor_idx,1] = e[4]
                targets[tensor_idx,2:4] = torch.tensor(e)[0:2]
                targets[tensor_idx,4:6] = torch.tensor(e)[0:2] + torch.tensor(e)[2:4]
                tensor_idx += 1
                
        self.mAP_acc_list += get_batch_statistics(predictions, targets, self.iou_threshold)
        self.mAP_acc_lables += targets[:, 1].tolist()
        
        
    def get_accur(self):
         #print("mAP_acc_list",self.mAP_acc_list)
         return calc_mAP(self.mAP_acc_list,self.mAP_acc_lables)
        
    
