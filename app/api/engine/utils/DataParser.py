#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 3/19/2020 8:54 AM $
# by : clu $
# SVN : $
#

import gc
import ast
import json
import h5py
import glob
import random
import scipy.io
from functools import partial
from hurry.filesize import size
from psutil import virtual_memory
from collections import namedtuple
from sklearn.model_selection import train_test_split
from .utils import *
import platform

from models.models import *

# used to load, configure and init transforms objects
import plugins.utils.pipeline
from torchvision.transforms import Compose

if platform.node() == 'AMURSK':
    SHARED_DATA_FOLDER = r"E:\SCCH_PROJECTS\ALOHA\trunk\TE_v2.0\TrainingEngine_v2_no_docker\data_folder"
else:
    SHARED_DATA_FOLDER = "/opt/www/shared_data/"

class DataParser:
    def __init__(self, config, alg, preprocessing_pipeline):
        self.config_handle = config

        self.project_id = config.project
        self.algorithm = alg
        
        self.designpoint_id = alg.get_id()
        self.data_set = config.data_set_name
        self.data_file = os.path.join(SHARED_DATA_FOLDER, config.data_file) if config.data_file else None
        self.data_dir = os.path.join(SHARED_DATA_FOLDER, config.data_folder) if config.data_folder else None
        self.num_classes = config.num_classes
        self.data_split = config.data_split
        self.cross_val = config.cross_val
        self.img_size = config.image_dimensions #HWC
        self.task_type = config.task_type

        self.data_file_already_splitted = False # FIXME: This value is hardcoded for the first tests
        self.preprocessing_pipeline_instance = plugins.utils.pipeline.Pipeline(preprocessing_pipeline)

        self.class_names = config.class_names
        self.sample_rate = config.sample_rate
        self.n_mels = config.n_mels
        self.silence_pcnt = config.silence_pcnt if config.silence_pcnt else 0
        self.bg_noise_pcnt = config.bg_noise_pcnt

        self.mean = None
        self.var = None

        self._path_init()


    def load_dataset(self):
        print('Start processing input data')

        self.file_name = self.data_set + '_projectID-{}_pipeline-{}.hdf5'.format(self.project_id, str(self.algorithm.pipeline))

        status = self._db_request()

        if status == 'exists':
            print('Dataset already exists. Retrieve information from the dataset')
            db_r = self._db_retrieve()
        else:
            print('Creating new dataset')
            db_r = self._parse_data()
        return db_r

    def _path_init(self):
        self.data_path = os.path.join(SHARED_DATA_FOLDER, "datasets")
        if not os.path.isdir(self.data_path):
            os.mkdir(self.data_path)
        self.experiment_path = os.path.join(SHARED_DATA_FOLDER, 'experiments', "prj_" + str(self.project_id),
                                            "alg_" + str(self.designpoint_id))
        if not os.path.isdir(self.experiment_path):
            os.makedirs(self.experiment_path)
        if not os.path.exists(os.path.join(self.data_path, "prj_" + str(self.project_id), self.data_set)):
            os.makedirs(os.path.join(self.data_path, "prj_" + str(self.project_id), self.data_set))

    def _parse_data(self):
        self.h5py_file_name = os.path.join(self.data_path, "prj_" + str(self.project_id), self.file_name)
        if self.data_set == 'MNIST':
            return self._load_MNIST()
        elif self.data_set == 'CIFAR10':
            return self._load_CIFAR10()
        elif self.data_set == 'CIFAR100':
            return self._load_CIFAR100()
        elif self.data_set in ["", " ", None]:
            raise ValueError('Dataset name should be defined')
        else:
            if self.data_file:
                return self._data_file_parse()
            elif self.data_dir:
                return self._data_folder_parse()
            else:
                raise ValueError('No data presented')

    def _data_file_parse(self):
        if os.path.splitext(self.data_file)[1] == '.txt':
            print("_data_file_parse txt")
            db_r = self._process_txt()
        elif os.path.splitext(self.data_file)[1] == '.json':
            print("_data_file_parse json")
            db_r = self._process_json()
        else:
            raise ValueError('Data format is not supported. Check documentation for data type support.')
        return db_r



#  ████████╗██╗  ██╗████████╗    ██████╗ ██████╗  ██████╗  ██████╗███████╗███████╗███████╗
#  ╚══██╔══╝╚██╗██╔╝╚══██╔══╝    ██╔══██╗██╔══██╗██╔═══██╗██╔════╝██╔════╝██╔════╝██╔════╝
#     ██║    ╚███╔╝    ██║       ██████╔╝██████╔╝██║   ██║██║     █████╗  ███████╗███████╗
#     ██║    ██╔██╗    ██║       ██╔═══╝ ██╔══██╗██║   ██║██║     ██╔══╝  ╚════██║╚════██║
#     ██║   ██╔╝ ██╗   ██║       ██║     ██║  ██║╚██████╔╝╚██████╗███████╗███████║███████║
#     ╚═╝   ╚═╝  ╚═╝   ╚═╝       ╚═╝     ╚═╝  ╚═╝ ╚═════╝  ╚═════╝╚══════╝╚══════╝╚══════╝
#                                                                                         


    def _process_txt(self):
        with open(self.data_file, 'r') as f:
            file_lines = f.readlines()

        if self.data_file_already_splitted: #FIXME: this is a temporary hack to use a predefined split, needed to make fair comparisons among experiments on the same dataset
            with open(self.data_file.replace('.txt','_validation.txt'), 'r') as f:
                file_lines_val = f.readlines()
            import random
            self.slice_split = [ random.sample([x for x in range(len(file_lines))],len(file_lines)) , random.sample([len(file_lines)+x for x in range(len(file_lines_val))], len(file_lines_val))]
            file_lines = file_lines + file_lines_val

        line = file_lines[0].rstrip()
        if len(line.split()) == 1:
            raise ValueError("Incorrect data representation")
        else:
            if self.task_type == "classification":
                if (os.path.splitext(line.split()[0])[1].lower() in ['.jpg', '.jpeg', '.png', '.bmp', '.tif', '.wav']) and \
                        line.rstrip().split()[1].isdigit():
                    print("Classification data detected. Integer encoded")
                    X_data_ = [os.path.join(self.data_dir, line.rstrip().split()[0]) for line in file_lines]
                    y_data = [int(line.rstrip().split()[1]) for line in file_lines]
                    y_data_ = one_hot_encode(y_data)
                elif (os.path.splitext(line.split()[0])[1].lower() in ['.jpg', '.jpeg', '.png', '.bmp', '.tif', '.wav']) and \
                        isinstance(ast.literal_eval(''.join(line.rstrip().split()[1:])), list) and \
                        (sum(ast.literal_eval(''.join(line.rstrip().split()[1:]))) == 1):
                    print("Classificaiton data detected. One hot encoded")
                    X_data_ = [os.path.join(self.data_dir, line.rstrip().split()[0]) for line in file_lines]
                    y_data_ = [ast.literal_eval(''.join(line.rstrip().split()[1:])) for line in file_lines]
                else:
                    raise ValueError("Incorrect data representation")
                if len(y_data_[0]) != self.num_classes:
                    self.num_classes = len(y_data_[0])
                    print('Wrong class defenition. Number of classes will be set to {}'.format(len(y_data_[0])))
            elif self.task_type == "segmentation":
                if (os.path.splitext(line.split()[0])[1].lower() in ['.jpg', '.jpeg', '.png', '.bmp', '.tif']) and (
                        os.path.splitext(line.rstrip().split()[1])[1].lower() in ['.jpg', '.jpeg', '.png', '.bmp', '.tif']):
                    X_data_ = [os.path.join(self.data_dir, line.rstrip().split()[0]) for line in file_lines]
                    y_data_ = [line.rstrip().split()[1] for line in file_lines]
                else:
                    raise ValueError("Incorrect data representation")
            else:
                raise ValueError('Such task not supported')
        if os.path.splitext(X_data_[0].split()[0])[1].lower() == '.wav':
            X_data_, y_data_ = self._add_silence(X_data_, y_data_)

        db_r = self._finalise(X_data_, y_data_, [len(X_data_)])
        return db_r



#       ██╗███████╗ ██████╗ ███╗   ██╗    ██████╗ ██████╗  ██████╗  ██████╗███████╗███████╗███████╗
#       ██║██╔════╝██╔═══██╗████╗  ██║    ██╔══██╗██╔══██╗██╔═══██╗██╔════╝██╔════╝██╔════╝██╔════╝
#       ██║███████╗██║   ██║██╔██╗ ██║    ██████╔╝██████╔╝██║   ██║██║     █████╗  ███████╗███████╗
#  ██   ██║╚════██║██║   ██║██║╚██╗██║    ██╔═══╝ ██╔══██╗██║   ██║██║     ██╔══╝  ╚════██║╚════██║
#  ╚█████╔╝███████║╚██████╔╝██║ ╚████║    ██║     ██║  ██║╚██████╔╝╚██████╗███████╗███████║███████║
#   ╚════╝ ╚══════╝ ╚═════╝ ╚═╝  ╚═══╝    ╚═╝     ╚═╝  ╚═╝ ╚═════╝  ╚═════╝╚══════╝╚══════╝╚══════╝
#                                                                                                  

    def _process_json(self):
        print("processing json file")
        if self.data_file == None:
          print("No annotation file has been specified. Looking for the default name")
          self.data_file = os.path.join(self.data_dir, "annotations.json")
          
        with open(self.data_file, 'r') as f:
            data = json.load(f)
        if self.task_type == "detection":
#            # max_debug_objects = 100
#            # iter = 0
#            max_objects = 0
#            X_data = []
#            y_data = []
#            class_mapping = [d['id'] for d in data['categories']]

#            if len(class_mapping) != self.num_classes:
#                self.num_classes = len(class_mapping)
#                print('Wrong class defenition. Number of classes will be set to {}'.format(len(class_mapping)))

#            del data['categories']
#            for d in data['images']:
#                X_data.append(os.path.join(self.data_dir, d['file_name']))
#                image_object = []
#                for a in data['annotations']:
#                    if d['id'] == a['image_id']:
#                        bb = self.process_bb(a['bbox'], d['width'], d['height'])
#                        image_object.append([class_mapping.index(a['category_id'])] + bb)
#                        data['annotations'].pop(data['annotations'].index(a))
#                if len(image_object) > max_objects:
#                    max_objects = len(image_object)
#                y_data.append(image_object)
#                # iter += 1
#                # if iter == max_debug_objects: break
#            for ann in y_data:
#                if len(ann) < max_objects:
#                    ann.extend([[0, 0, 0, 0, 0]]*(max_objects-len(ann)))
            X_data_ = [os.path.join(self.data_dir, data['source_path'], d['frame']) for d in data['meta']]
            y_list = []
            for d in data['meta']:
                lst = []
                if d['height']>d['width']:
                  imgheight_rate = self.img_size[0]/d['height']
                  imgwidth_rate  = imgheight_rate
                  h_offset = 0
                  w_offset = (self.img_size[1] - imgwidth_rate * d['width'])/2
                else:
                  imgwidth_rate  = self.img_size[1]/d['width']
                  imgheight_rate = imgwidth_rate
                  h_offset = (self.img_size[0] - imgheight_rate * d['height'])/2
                  w_offset = 0
                  
                  
#                        imgwidth_rate = self.img_size[1]/d['width']
#                        imgheight_rate = self.img_size[0]/d['height']
    #            print ("===========================")
    #            print ("self.img_size[0]")
    #            print (self.img_size[0])

    #            print ("self.img_size[1]")
    #            print (self.img_size[1])

    #            print ("d['width']")
    #            print (d['width'])

    #            print ("d['height']")
    #            print (d['height'])

    #            print ("imgwidth_rate")
    #            print (imgwidth_rate)
                
    #            print ("imgheight_rate")
    #            print (imgheight_rate)
                
    #            print ("w_offset")
    #            print (w_offset)
                
    #            print ("h_offset")
    #            print (h_offset)
                
                for o in d['objects']:
                    obb = o['bb']
                    
                    x1y1x2y2_format = False
                    if x1y1x2y2_format:
                      lst.append([obb[0] * imgwidth_rate + w_offset, #x1
                                  obb[1] * imgheight_rate + h_offset, #y1
                                  obb[2] * imgwidth_rate+ w_offset, #x2
                                  obb[3] * imgheight_rate + h_offset, #y2
                                  o['object_class']]) #class
                    else:
                      lst.append([obb[0]            * imgwidth_rate  + w_offset, #x
                                   obb[1]            * imgheight_rate + h_offset, #y
                                   (obb[2] - obb[0]) * imgwidth_rate, #w
                                   (obb[3] - obb[1]) * imgheight_rate, #h
                                   o['object_class']]) #class
                y_list.append(lst)
          #  print (y_list)
            ll = max([len(l) for l in y_list])
            y_data_ = []
            for l in y_list:
                m = l
                for i in range(ll - len(m)):
                    m.append([0, 0, 0, 0, 0])
                y_data_.append(m)
                #print(m)
            y_data_ = np.array(y_data_)
         #   print("y_data_.shape")
         #   print(y_data_.shape)
            
            X_data = X_data_
            y_data = y_data_

        else:
            return NotImplementedError

        db_r = self._finalise(X_data, y_data, [len(X_data)])
        return db_r

    def process_bb(self, bbox, im_w, im_h):
        max_dim = max([im_w, im_h])
        dim_diff = np.abs(im_w - im_h)
        pad1 = dim_diff/2
        pad2 = dim_diff - dim_diff/2

        if im_h <= im_w:
            x1 = bbox[0]
            y1 = bbox[1] + pad1
            x2 = bbox[0] + bbox[2]
            y2 = bbox[1] + bbox[3] + pad2
        else:
            x1 = bbox[0] + pad1
            y1 = bbox[1]
            x2 = bbox[0] + bbox[2] + pad2
            y2 = bbox[1] + bbox[3]

        return [((x1+x2)/2)/max_dim, ((y1+y2)/2)/max_dim, bbox[2]/max_dim, bbox[3]/max_dim]


#  ███████╗ ██████╗ ██╗     ██████╗ ███████╗██████╗     ██████╗ ██████╗  ██████╗  ██████╗███████╗███████╗███████╗
#  ██╔════╝██╔═══██╗██║     ██╔══██╗██╔════╝██╔══██╗    ██╔══██╗██╔══██╗██╔═══██╗██╔════╝██╔════╝██╔════╝██╔════╝
#  █████╗  ██║   ██║██║     ██║  ██║█████╗  ██████╔╝    ██████╔╝██████╔╝██║   ██║██║     █████╗  ███████╗███████╗
#  ██╔══╝  ██║   ██║██║     ██║  ██║██╔══╝  ██╔══██╗    ██╔═══╝ ██╔══██╗██║   ██║██║     ██╔══╝  ╚════██║╚════██║
#  ██║     ╚██████╔╝███████╗██████╔╝███████╗██║  ██║    ██║     ██║  ██║╚██████╔╝╚██████╗███████╗███████║███████║
#  ╚═╝      ╚═════╝ ╚══════╝╚═════╝ ╚══════╝╚═╝  ╚═╝    ╚═╝     ╚═╝  ╚═╝ ╚═════╝  ╚═════╝╚══════╝╚══════╝╚══════╝
#                                                                                                                

    def _data_folder_parse(self):
        path_list = [i for i in os.listdir(self.data_dir) if os.path.isdir(os.path.join(self.data_dir, i))]
        if self.task_type == 'classification':
            if self.class_names == None:
                raise ValueError('No classes defined')
            else:
                if set(path_list) >= set(self.class_names):
                    X_data_ = []
                    y_data = []
                    for cls in self.class_names:
                        file_names = [os.path.join(self.data_dir, cls, i) for i in os.listdir(os.path.join(self.data_dir, cls))]
                        X_data_ += file_names
                        y_data += [self.class_names.index(cls)]*len(file_names)
                    y_data_ = one_hot_encode(y_data)

                    if len(self.class_names) != self.num_classes:
                        self.num_classes = len(self.class_names)
                        print('Wrong class defenition. Number of classes will be set to {}'.format(len(y_data_[0])))
                    if os.path.splitext(X_data_[0].split()[0])[1].lower() == '.wav':
                        X_data_, y_data_ = self._add_silence(X_data_, y_data_)
                    db_r = self._finalise(X_data_, y_data_, [len(X_data_)])
                else:
                    raise ValueError('No classes found in folder names')
        elif self.task_type == 'segmentation':
            if len(path_list) == 0:
                file_list = [os.path.join(self.data_dir, i) for i in os.listdir(self.data_dir)]
                if os.path.splitext(file_list[0].split()[0])[1].lower() == '.mat' and len(file_list) > 1:
                    with h5py.File(self.h5py_file_name, 'a') as f:
                        dset_x = f.create_dataset('X_data',
                                                  (0, self.img_size[0], self.img_size[1], self.img_size[2]),
                                                  maxshape=(None, self.img_size[0], self.img_size[1], self.img_size[2]), dtype='uint8')
                        dset_y = f.create_dataset('y_data',
                                                  (0, self.img_size[0], self.img_size[1], 1),
                                                  maxshape=(None, self.img_size[0], self.img_size[1], 1))
                        try:
                            mean_aver = []
                            var_aver = []
                            data_len = []
                            max_lbl = 0
                            curr_max = 0
                            for i in file_list:
                                file_mat = scipy.io.loadmat(i)
                                X_data = file_mat['slices']
                                if np.max(X_data[0]) > 1:
                                    X_data = X_data / 255
                                y_data = file_mat['masks']

                                mean_aver.append(X_data)
                                var_aver.append(X_data)

                                data_len.append([i for i in range(curr_max, X_data.shape[2]+curr_max)])
                                curr_max = X_data.shape[2]-1
                                dset_x.resize(dset_x.shape[0] + X_data.shape[2], axis=0)
                                dset_y.resize(dset_y.shape[0] + y_data.shape[2], axis=0)

                                im_tr = np.transpose(X_data, (2, 0, 1))
                                im_r = np.asarray(
                                    [cv2.resize(kk, (self.img_size[0], self.img_size[1])) for kk in im_tr])
                                ms_tr = np.transpose(y_data, (2, 0, 1))
                                ms_r = np.asarray(
                                    [cv2.resize(kk, (self.img_size[0], self.img_size[1])) for kk in ms_tr])

                                dset_x[-X_data.shape[2]:] = np.expand_dims(im_r, -1)
                                dset_y[-y_data.shape[2]:] = ms_r
                                if np.max(ms_r) > max_lbl:
                                    max_lbl = np.max(ms_r)
                            print("Dataset to h5py file dump finished")
                            if len(mean_aver) > 1:
                                self.mean = np.mean(np.array(mean_aver), axis=0)
                                self.var = np.mean(np.array(var_aver), axis=0)
                            else:
                                self.mean = np.mean(np.array(mean_aver), axis=1)
                                self.var = np.mean(np.array(var_aver), axis=1)

                            if max_lbl != self.num_classes:
                                print('Wrong class defenition. Number of classes will be set to {}'.format(max_lbl))
                                self.num_classes = max_lbl
                            else:
                                print('Number of classes will be set to {}'.format(self.num_classes))

                            self.slice_split = self._cross_val_split(data_len)
                            db_r = self._db_dump()
                        except:
                            raise ValueError("can't dump file")
                elif os.path.splitext(file_list[0].split()[0])[1].lower() in ['.jpg', '.jpeg', '.png', '.bmp', '.tif']:
                    y_data_ = glob.glob(os.path.join(self.data_dir, "*_mask.*"))
                    X_data_ = [msk.replace('_mask', '') for msk in y_data_]
                    db_r = self._finalise(X_data_, y_data_, [len(X_data_)])
                else:
                    raise ValueError('Unsupported data format')
            elif len(path_list) >= 2:
                if "images" in path_list and "masks" in path_list:
                    X_data_ = path_walk(os.path.join(self.data_dir, 'images'))
                    y_data_ = path_walk(os.path.join(self.data_dir, 'masks'))
                    db_r = self._finalise(X_data_, y_data_, [len(X_data_)])
                else:
                    data_len = []
                    curr_max = 0
                    X_data = []
                    y_data = []
                    for pth in path_list:
                        y_data_ = glob.glob(os.path.join(self.data_dir, pth, "*_mask.*"))
                        y_data.extend(y_data_)
                        X_data.extend([msk.replace('_mask', '') for msk in y_data_])
                        data_len.append([i for i in range(curr_max, len(y_data_) + curr_max)])
                        curr_max += len(y_data_) #- 1
                    db_r = self._finalise(X_data, y_data, data_len)
            else:
                raise ValueError('No data found')
        return db_r



#  ██████╗ ██╗   ██╗███╗   ███╗██████╗ 
#  ██╔══██╗██║   ██║████╗ ████║██╔══██╗
#  ██║  ██║██║   ██║██╔████╔██║██████╔╝
#  ██║  ██║██║   ██║██║╚██╔╝██║██╔═══╝ 
#  ██████╔╝╚██████╔╝██║ ╚═╝ ██║██║     
#  ╚═════╝  ╚═════╝ ╚═╝     ╚═╝╚═╝     
#                                      


    def _finalise(self, X_data_, y_data_, data_size):
        if not self.data_file_already_splitted:
            print("creating cross-val split")
            self.slice_split = self._cross_val_split(data_size)
        self.data_size = len(X_data_)
        self._dump_h5py(X_data_, y_data_)
        self.dict_data_path = self.h5py_file_name
        db_r = self._db_dump()
        return db_r

    def _dump_h5py(self, x_list, y_list):
        memory_perc = 0.8
        print("calculating memory capacity")
        mem = virtual_memory()
        print('Memory available {}'.format(size(mem.available)))
        free_mem = int(mem.available * 0.8)
        print('The {}% of memory will be used for dataset processing: {}'.format(memory_perc*100, size(free_mem)))
        
        
#     ____ _        _    ____ ____ ___ _____ ___ ____    _  _____ ___ ___  _   _ 
#    / ___| |      / \  / ___/ ___|_ _|  ___|_ _/ ___|  / \|_   _|_ _/ _ \| \ | |
#   | |   | |     / _ \ \___ \___ \| || |_   | | |     / _ \ | |  | | | | |  \| |
#   | |___| |___ / ___ \ ___) |__) | ||  _|  | | |___ / ___ \| |  | | |_| | |\  |
#    \____|_____/_/   \_\____/____/___|_|   |___\____/_/   \_\_| |___\___/|_| \_|
#                                                                                

        if self.task_type == 'classification':
            x_list_split, splits = prepare_memory_splits(free_mem, x_list)

            # Load plugins to perform preprocessing sample by sample
            print("[Preprocessing pipeline] Load sample transforms")
            sample_transforms = self.preprocessing_pipeline_instance.get_transforms('sample_transforms')
            if len(sample_transforms) < 1:
                print("Error! At least one sample preprocessing must be specified currently, this may change in the future")
                raise ValueError

            sample_transforms_composed = Compose(sample_transforms)

            # Load plugins to perform preprocessing on entire dataset
            print("[Preprocessing pipeline] Load dataset transforms, if any")
            pre_dataset_transforms = self.preprocessing_pipeline_instance.get_transforms('dataset_transforms')

            sample_pipe_test_out = sample_transforms_composed(x_list_split[0][0])
            
            
            print("[SampleTransforms] Executing sample transforms")
            with h5py.File(self.h5py_file_name, 'a') as f:
                print("Data splited in {:d} chunks".format(splits))
                f.create_dataset('y_data', data=y_list)
                dset_x = f.create_dataset('X_data',
                                            (0, *(sample_pipe_test_out.shape)),
                                            maxshape=(None, *(sample_pipe_test_out.shape)),
                                            chunks=True, dtype='uint8')

                for i in range(len(x_list_split)):
                    print("Start data process loop " + str(i + 1))
                    pool = mp.Pool(mp.cpu_count())
                    dset_x.resize(dset_x.shape[0] + len(x_list_split[i]), axis=0)
                    out = pool.map(sample_transforms_composed, x_list_split[i])
                    
                    out = np.array(out)

                    dset_x[-len(x_list_split[i]):] = out
                    pool.close()
                    pool.join()

                # apply dataset preprocessing plugins
                # TODO this kind of transforms should know if they are working on the training set or on the validation set probably
                # to work 
                count = 0
                if pre_dataset_transforms:
                    for plugin in pre_dataset_transforms:
                        count = count + 1
                        print("[DatasetTransforms] Executing transform {}/{}".format(count, len(pre_dataset_transforms)))
                        dset_x = plugin.apply(dset_x, self.slice_split)

            status = "File successfully saved in: {:s}".format(self.h5py_file_name)
            print(status)
            gc.collect()
            return status



#    ____  _____ ____ __  __ _____ _   _ _____  _  _____ ___ ___  _   _ 
#   / ___|| ____/ ___|  \/  | ____| \ | |_   _|/ \|_   _|_ _/ _ \| \ | |
#   \___ \|  _|| |  _| |\/| |  _| |  \| | | | / _ \ | |  | | | | |  \| |
#    ___) | |__| |_| | |  | | |___| |\  | | |/ ___ \| |  | | |_| | |\  |
#   |____/|_____\____|_|  |_|_____|_| \_| |_/_/   \_\_| |___\___/|_| \_|
#                                                                       

        elif self.task_type == 'segmentation':
            x_list_split, x_splits = prepare_memory_splits(free_mem, x_list)
            y_list_split, y_splits = prepare_memory_splits(free_mem, y_list)

            # Load plugins to perform preprocessing sample by sample
            print("[Preprocessing pipeline] Load sample transforms")
            sample_transforms     = self.preprocessing_pipeline_instance.get_transforms('sample_transforms')
            sample_transforms_lbl = self.preprocessing_pipeline_instance.get_transforms('sample_transforms_lbl')
            if len(sample_transforms) < 1:
                print("Error! At least one sample preprocessing must be specified currently, this may change in the future")
                raise ValueError

            sample_transforms_composed     = Compose(sample_transforms)
            sample_transforms_composed_lbl = Compose(sample_transforms_lbl)



            # Load plugins to perform preprocessing on entire dataset
            print("[Preprocessing pipeline] Load dataset transforms, if any")
            pre_dataset_transforms     = self.preprocessing_pipeline_instance.get_transforms('dataset_transforms')
            pre_dataset_transforms_lbl = self.preprocessing_pipeline_instance.get_transforms('dataset_transforms_lbl')

            sample_pipe_test_out = sample_transforms_composed    (x_list_split[0][0])
            label_pipe_test_out  = sample_transforms_composed_lbl(y_list_split[0][0])
            
            
            



            with h5py.File(self.h5py_file_name, 'a') as f:
                dset_x = f.create_dataset('X_data',
                                          (0, *(sample_pipe_test_out.shape)),
                                          maxshape=(None, *(sample_pipe_test_out.shape)), dtype='uint8')
                dset_y = f.create_dataset('y_data',
                                          (0, *(label_pipe_test_out.shape)),
                                          maxshape=(None, *(label_pipe_test_out.shape)), dtype='int32')
                
                
                print("Data splited in {} chunks".format(x_splits))
                mean_aver = []
                var_aver = []
                max_lbl = 0
                for i in range(len(x_list_split)):
                    print("Start image process loop " + str(i + 1))
                    pool = mp.Pool(mp.cpu_count())

                    dset_x.resize(dset_x.shape[0] + len(x_list_split[i]), axis=0)
                    out = pool.map(sample_transforms_composed, x_list_split[i])
                    out = np.array(out)
                    dset_x[-len(x_list_split[i]):] = out
                    pool.close()
                    pool.join()
                
                
                print("Label data splited in {} chunks".format(y_splits))
                for i in range(len(y_list_split)):
                    print("Start mask process loop " + str(i + 1))
                    pool = mp.Pool(mp.cpu_count())

                    dset_y.resize(dset_y.shape[0] + len(y_list_split[i]), axis=0)
                    out = pool.map(sample_transforms_composed_lbl, y_list_split[i])
                    out = np.array(out)
                    dset_y[-len(y_list_split[i]):] = out
                    pool.close()
                    pool.join()
                    if np.max(out) > max_lbl:
                        max_lbl = np.max(out)
               
               
                if max_lbl != self.num_classes:
                    print('Wrong class defenition. Number of classes will be set to {}'.format(max_lbl))
                    self.num_classes = max_lbl
                else:
                    print('Number of classes will be set to {}'.format(self.num_classes))




            status = "File successfully saved in: {:s}".format(self.h5py_file_name)

            print(status)
            gc.collect()
            return status
            
            
            
#    ____  _____ _____ _____ ____ _____ ___ ___  _   _ 
#   |  _ \| ____|_   _| ____/ ___|_   _|_ _/ _ \| \ | |
#   | | | |  _|   | | |  _|| |     | |  | | | | |  \| |
#   | |_| | |___  | | | |__| |___  | |  | | |_| | |\  |
#   |____/|_____| |_| |_____\____| |_| |___\___/|_| \_|
#                                                      

            
        elif self.task_type == 'detection':
            x_list_split, splits = prepare_memory_splits(free_mem, x_list)

            
            # Load plugins to perform preprocessing sample by sample
            print("[Preprocessing pipeline] Load sample transforms")
            sample_transforms = self.preprocessing_pipeline_instance.get_transforms('sample_transforms')
            if len(sample_transforms) < 1:
                print("Error! At least one sample preprocessing must be specified currently, this may change in the future")
                raise ValueError

            sample_transforms_composed = Compose(sample_transforms)

            # Load plugins to perform preprocessing on entire dataset
            print("[Preprocessing pipeline] Load dataset transforms, if any")
            pre_dataset_transforms = self.preprocessing_pipeline_instance.get_transforms('dataset_transforms')

            sample_pipe_test_out = sample_transforms_composed(x_list_split[0][0])
            
            if len(sample_pipe_test_out.shape)<3:
              sample_pipe_test_out = np.expand_dims(sample_pipe_test_out,axis=-1)

            
            print("[SampleTransforms] Executing sample transforms")
            
            with h5py.File(self.h5py_file_name, 'a') as f:
                print("Data splited in {:d} chunks".format(splits))
                f.create_dataset('y_data', data=y_list)
                dset_x = f.create_dataset('X_data',
                                          (0, *(sample_pipe_test_out.shape)),
                                          maxshape=(None, *(sample_pipe_test_out.shape)),
                                          chunks=True, dtype='uint8')
                mean_aver = []
                var_aver = []
                padding = []
                original_image_size = []
                for i in range(len(x_list_split)):
                    print("Start data process loop " + str(i + 1))
                    pool = mp.Pool(mp.cpu_count())
                    #func = partial(bulk_process_detection, self.img_size)
                    # temp_map = np.array(pool.map(func, x_list_split[i]))
                    dset_x.resize(dset_x.shape[0] + len(x_list_split[i]), axis=0)
                    out = pool.map(sample_transforms_composed, x_list_split[i])
                    out = np.array(out)
                    
                    dset_x[-len(x_list_split[i]):] = out
#                    mean_aver.extend([item[1] for item in out])
#                    var_aver.extend([item[2] for item in out])
#                    padding.append([item[3] for item in out])
#                    original_image_size.append([item[4] for item in out])
                    pool.close()
                    pool.join()
            status = "File successfully saved in: {:s}".format(self.h5py_file_name)
            
            
            print(status)
            gc.collect()
            return status

        else:
            raise ValueError('no such task found')







#  ███╗   ███╗███╗   ██╗██╗███████╗████████╗
#  ████╗ ████║████╗  ██║██║██╔════╝╚══██╔══╝
#  ██╔████╔██║██╔██╗ ██║██║███████╗   ██║   
#  ██║╚██╔╝██║██║╚██╗██║██║╚════██║   ██║   
#  ██║ ╚═╝ ██║██║ ╚████║██║███████║   ██║   
#  ╚═╝     ╚═╝╚═╝  ╚═══╝╚═╝╚══════╝   ╚═╝   
#                                           

    def _load_MNIST(self):
        if not os.path.isfile(self.h5py_file_name):
            print('Creating h5py file for MNIST')

            import torchvision.datasets as datasets
            mnist_trainset = datasets.MNIST(root=os.path.join(self.data_path), train=True, download=True,
                                            transform=None)

        import struct
        with open(os.path.join(self.data_path, self.data_set + r'/raw/train-images-idx3-ubyte'), 'rb') as imgpath:
            magic, num, rows, cols = struct.unpack(">IIII", imgpath.read(16))
            X_data = np.fromfile(imgpath, dtype=np.uint8).reshape((-1, 28, 28, 1))

        with open(os.path.join(self.data_path, self.data_set + r'/raw/train-labels-idx1-ubyte'), 'rb') as lbpath:
            magic, n = struct.unpack('>II', lbpath.read(8))
            y_data = np.fromfile(lbpath, dtype=np.uint8).tolist()

        tr_l = len(y_data)

        with open(os.path.join(self.data_path, self.data_set + r'/raw/t10k-images-idx3-ubyte'), 'rb') as imgpath:
            magic, num, rows, cols = struct.unpack(">IIII", imgpath.read(16))
            X_data_ = np.fromfile(imgpath, dtype=np.uint8).reshape((-1, 28, 28, 1))

        with open(os.path.join(self.data_path, self.data_set + r'/raw/t10k-labels-idx1-ubyte'), 'rb') as lbpath:
            magic, n = struct.unpack('>II', lbpath.read(8))
            y_data_ = np.fromfile(lbpath, dtype=np.uint8).tolist()

        X_data = np.concatenate((X_data, X_data_), axis=0)
        y_data.extend(y_data_)

        if np.max(X_data[0]) == 255:
            X_data = X_data/255

        if self.num_classes != 10:
            print('Wrong class definition for MNIST case. Value is et to 10')
            self.num_classes = 10

        self.mean = np.mean(X_data, axis=(0, 1, 2)).tolist()
        self.var = np.var(X_data, axis=(0, 1, 2)).tolist()

        print("Dataset to h5py file dump starts")
        try:
            if self.cross_val < 2:
                self.slice_split = [[i for i in range(tr_l)], [i for i in range(tr_l, len(y_data))]]
            else:
                self.slice_split = self._cross_val_split([len(y_data)])

            with h5py.File(self.h5py_file_name, 'w') as f:
                f.create_dataset('X_data', data=X_data)
                f.create_dataset('y_data', data=one_hot_encode(y_data))
            print("Dataset to h5py file dump finished")
        except KeyError:
            raise ValueError("Unable to dump file")

        db_r = self._db_dump()
        return db_r




#   ██████╗██╗███████╗ █████╗ ██████╗ 
#  ██╔════╝██║██╔════╝██╔══██╗██╔══██╗
#  ██║     ██║█████╗  ███████║██████╔╝
#  ██║     ██║██╔══╝  ██╔══██║██╔══██╗
#  ╚██████╗██║██║     ██║  ██║██║  ██║
#   ╚═════╝╚═╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝
#                                     


    def _load_CIFAR10(self):
        import torchvision.datasets as datasets
        cifar_trainset = datasets.CIFAR10(root=os.path.join(self.data_path, self.data_set), train=True, download=True,
                                          transform=None)
        cifar_validset = datasets.CIFAR10(root=os.path.join(self.data_path, self.data_set), train=False, download=True,
                                          transform=None)

        X_data = [np.asarray(x) for x, _ in cifar_trainset]
        X_data.extend([np.asarray(x) for x, _ in cifar_validset])
        y_data = [x for _, x in cifar_trainset]
        tr_l = len(y_data)
        y_data.extend([x for _, x in cifar_validset])

        if self.num_classes != 10:
            print('Wrong class definition for CIFAR10 case. Value is et to 10')
            self.num_classes = 10

        if np.max(X_data[0]) == 255:
            X_data = np.array(X_data)/255

        self.mean = np.mean(X_data, axis=(0, 1, 2)).tolist()
        self.var = np.var(X_data, axis=(0, 1, 2)).tolist()

        print("Dataset to h5py file dump starts")
        try:
            if self.cross_val < 2:
                self.slice_split = [[i for i in range(tr_l)], [i for i in range(tr_l, len(y_data))]]
            else:
                self.slice_split = self._cross_val_split([len(y_data)])

            with h5py.File(self.h5py_file_name, 'w') as f:
                f.create_dataset('X_data', data=X_data)
                f.create_dataset('y_data', data=one_hot_encode(y_data))
            print("Dataset to h5py file dump finished")
        except KeyError:
            raise ValueError("Unable to dump file")

        db_r = self._db_dump()
        return db_r

    def _load_CIFAR100(self):
        import torchvision.datasets as datasets
        cifar_trainset = datasets.CIFAR100(root=os.path.join(self.data_path, self.data_set), train=True, download=True,
                                           transform=None)
        cifar_validset = datasets.CIFAR100(root=os.path.join(self.data_path, self.data_set), train=False, download=True,
                                           transform=None)
        X_data = [np.asarray(x) for x, _ in cifar_trainset]
        X_data.extend([np.asarray(x) for x, _ in cifar_validset])
        y_data = [x for _, x in cifar_trainset]
        tr_l = len(y_data)
        y_data.extend([x for _, x in cifar_validset])

        if np.max(X_data[0]) == 255:
            X_data = np.asarray(X_data)/255

        if self.num_classes != 100:
            print('Wrong class definition for CIFAR10 case. Value is et to 100')
            self.num_classes = 100

        self.mean = np.mean(X_data, axis=(0, 1, 2)).tolist()
        self.var = np.var(X_data, axis=(0, 1, 2)).tolist()

        print("Dataset to h5py file dump starts")
        try:
            if self.cross_val < 2:
                self.slice_split = [[i for i in range(tr_l)], [i for i in range(tr_l, len(y_data))]]
            else:
                self.slice_split = self._cross_val_split([len(y_data)])
            with h5py.File(self.h5py_file_name, 'w') as f:
                f.create_dataset('X_data', data=np.asarray(X_data))
                f.create_dataset('y_data', data=one_hot_encode(y_data))
            print("Dataset to h5py file dump finished")
        except KeyError:
            raise ValueError("Unable to dump file")

        db_r = self._db_dump()
        return db_r

    def _check_data_split(self):
        if self.data_split:
            if self.data_split > 1 or self.data_split < 0.1:
                raise ValueError('Incorrect data_split value')
        else:
            self.data_split = 0.7
            print("Assigned standard data split value of 0.7")

    def _cross_val_split(self, data_dim):
        if len(data_dim) == 1:
            ind = random.sample([i for i in range(data_dim[0])], data_dim[0])
            if self.cross_val > 2 and self.cross_val < 10:
                cross_val_patch_size = round(data_dim[0] / self.cross_val)
                slices = [ind[i:i + cross_val_patch_size] for i in range(0, data_dim[0], cross_val_patch_size)]
            else:
                print("Cross-validation is disabled. Will use data split instead")
                if not self.data_split and self.data_split > 1 and self.data_split < 0.1:
                    print("Data split set up to 0.3")
                    self.data_split = 0.7
                    ind_train, ind_val = train_test_split(ind, train_size=self.data_split)
                else:
                    ind_train, ind_val = train_test_split(ind, train_size=self.data_split)
                slices = [ind_train, ind_val]
        else:
            ind = random.sample([i for i in range(len(data_dim))], len(data_dim))
            if self.cross_val > 2 and self.cross_val < 10:
                cross_val_patch_size = round(len(data_dim) / self.cross_val)
                slices_ = [ind[i:i + cross_val_patch_size] for i in range(0, len(data_dim), cross_val_patch_size)]
                slices = [sum([data_dim[slices_[i][k]] for k in range(len(slices_[i]))], []) for i in range(len(slices_))]
            else:
                print("Cross-validation is disabled. Will use data split instead")
                if not self.data_split and self.data_split > 1 and self.data_split < 0.1:
                    print("Data split set up to 0.3")
                    self.data_split = 0.7
                    ind_train, ind_val = train_test_split(ind, train_size=self.data_split)
                else:
                    ind_train, ind_val = train_test_split(ind, train_size=self.data_split)
                slices = [sum([data_dim[i] for i in ind_train], []), sum([data_dim[i] for i in ind_val], [])]
        return slices

    def _add_silence(self, x_list, y_list):
        if self.silence_pcnt > 0 and self.silence_pcnt < 0.5:
            print('Adding silence')
            x_list += [''] * int(len(x_list) * self.silence_pcnt)
            y_list = [item + [0] for item in y_list]
            y_list += one_hot_encode([self.num_classes] * int(len(y_list) * self.silence_pcnt))
            self.num_classes += 1
            print('Number of classes: {}'.format(self.num_classes))
            if self.class_names != None:
                self.class_names += ['silence']
        return x_list, y_list

    def _db_request(self):
        try:
            Dataset.objects.get(path=self.file_name)
            return 'exists'
        except Dataset.DoesNotExist:
            return 'none'

    def _db_retrieve(self):
        return 'exists'

    def _db_dump(self):
        dataset = Dataset()
        dataset.name = self.file_name

        dataset.project  = str(self.config_handle.project)
        dataset.pipeline = str(self.algorithm.pipeline)

        dataset.path        = self.h5py_file_name.replace (SHARED_DATA_FOLDER,'')
        dataset.num_classes = self.num_classes
        
        dataset.mean        = self.mean
        dataset.var         = self.var
        
        dataset.slice_split = self.slice_split
        dataset.save()
        
        train_valid_split = os.path.join(os.path.split(self.h5py_file_name)[0],"train_valid_split.py")
        with open(train_valid_split, 'w') as f:
          f.write("# Subdivision in training split and validation split")
          f.write("train_valid_split = " + str(self.slice_split))
          f.write("filename = " + str(os.path.split(self.h5py_file_name)[1]))
        
        train_valid_split = os.path.join(os.path.split(self.h5py_file_name)[0],"train_valid_split.json")
        with open(train_valid_split, 'w') as f:
          dict_split = {'train_valid_split':self.slice_split}
          f.write(json.dumps(dict_split))
         
          
          
        self.config_handle.num_classes = self.num_classes
        self.config_handle.save()

        return 'created'
