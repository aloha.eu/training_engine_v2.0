#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 3/30/2020 9:21 AM $
# by : clu $
# SVN : $
#

import os
import glob
import random
import librosa
import numpy as np

from torchvision import transforms
from skimage.transform import rescale, rotate

import platform
if platform.node() == 'AMURSK':
    SHARED_DATA_FOLDER = r"E:\SCCH_PROJECTS\ALOHA\trunk\TE_v2.0\TrainingEngine_v2_no_docker\data_folder"
else:
    SHARED_DATA_FOLDER = "/opt/www/shared_data/"

class HorizontalFlip(object):
    def __init__(self, flip_prob, key):
        self.flip_prob = flip_prob
        self.key = key

    def __call__(self, sample):
        x, y = sample
        if np.random.rand() > self.flip_prob:
            return [x, y]
        else:
            if self.key == 'classification':
                x = np.fliplr(x).copy()
                return [x, y]
            elif self.key == 'segmentation':
                x = np.fliplr(x).copy()
                y = np.fliplr(y).copy()
                return [x, y]
            elif self.key == 'detection':
                raise NotImplementedError
                # x = np.fliplr(x).copy()
                # return [x, y]


class VerticalFlip(object):
    def __init__(self, flip_prob, key):
        self.flip_prob = flip_prob
        self.key = key

    def __call__(self, sample):
        x, y = sample
        if np.random.rand() > self.flip_prob:
            return [x, y]
        else:
            if self.key == 'classification':
                x = np.flipud(x).copy()
                return [x, y]
            elif self.key == 'segmentation':
                x = np.flipud(x).copy()
                y = np.flipud(y).copy()
                return [x, y]
            elif self.key == 'detection':
                raise NotImplementedError



class Normalize(object):
    def __init__(self, mean, var):
        self.mean = mean
        self.var = var

    def __call__(self, sample):
        x, y = sample
        x = (x-self.mean)/self.var
        return [x, y]


class Scale(object):
    def __init__(self, scale, key):
        self.scale = scale
        self.key = key

    def __call__(self, sample):
        x, y = sample
        img_size = x.shape[0]
        scale = np.random.uniform(low=1.0 - self.scale, high=1.0 + self.scale)
        if self.key == 'classification':
            x = rescale(x, (scale, scale), multichannel=True, preserve_range=True, mode="constant", anti_aliasing=False)
            if scale < 1.0:
                diff = (img_size - x.shape[0]) / 2.0
                padding = ((int(np.floor(diff)), int(np.ceil(diff))),) * 2 + ((0, 0),)
                x = np.pad(x, padding, mode="constant", constant_values=0)
            else:
                x_min = (x.shape[0] - img_size) // 2
                x_max = x_min + img_size
                x = x[x_min:x_max, x_min:x_max, ...]
            return [x, y]
        elif self.key == 'segmentation':
            x = rescale(x, (scale, scale), multichannel=True, preserve_range=True, mode="constant", anti_aliasing=False)
            y = rescale(y, (scale, scale), order=0, multichannel=True, preserve_range=True, mode="constant", anti_aliasing=False)
            if scale < 1.0:
                diff = (img_size - x.shape[0]) / 2.0
                padding = ((int(np.floor(diff)), int(np.ceil(diff))),) * 2 + ((0, 0),)
                x = np.pad(x, padding, mode="constant", constant_values=0)
                y = np.pad(y, padding, mode="constant", constant_values=0)
            else:
                x_min = (x.shape[0] - img_size) // 2
                x_max = x_min + img_size
                x = x[x_min:x_max, x_min:x_max, ...]
                y = y[x_min:x_max, x_min:x_max, ...]
            return [x, y]
        elif self.key == 'detection':
            raise NotImplementedError


class Rotate(object):
    def __init__(self, angle, key):
        self.angle = angle
        self.key = key

    def __call__(self, sample):
        x, y = sample
        angle = np.random.uniform(low=-self.angle, high=self.angle)
        if self.key == 'classification':
            x = rotate(x, angle, resize=False, preserve_range=True, mode="constant")
            return [x, y]
        elif self.key == 'segmentation':
            x = rotate(x, angle, resize=False, preserve_range=True, mode="constant")
            y = rotate(y, angle, resize=False, order=0, preserve_range=True, mode="constant")
            return [x, y]
        elif self.key == 'detection':
            raise NotImplementedError


class AudioAugmentation(object):
    def __init__(self, n_fft=2048, hop_length=512, max_scale=0.2, max_shift=8):
        self.n_fft = n_fft
        self.hop_length = hop_length
        self.max_scale = max_scale
        self.max_shift = max_shift

    def __call__(self, sample):
        x, y = sample
        # to STFT
        stft = librosa.stft(x, n_fft=self.n_fft, hop_length=self.hop_length)
        stft_shape = stft.shape
        # stretch audio
        scale = random.uniform(-self.max_scale, self.max_scale)
        stft = librosa.core.phase_vocoder(stft, 1+scale, hop_length=self.hop_length)
        # shift audio
        shift = random.randint(-self.max_shift, self.max_shift)
        a = -min(0, shift)
        b = max(0, shift)
        stft = np.pad(stft, ((0, 0), (a, b)), "constant")
        if a == 0:
            stft = stft[:, b:]
        else:
            stft = stft[:, 0:-a]
        # fix length
        t_len = stft.shape[1]
        orig_t_len = stft_shape[1]
        if t_len > orig_t_len:
            stft = stft[:, 0:orig_t_len]
        elif t_len < orig_t_len:
            stft = np.pad(stft, ((0, 0), (0, orig_t_len-t_len)), "constant")
        return [stft, y]


class AddBGNoise(object):
    def __init__(self, sample_rate=16000, noise_prob=0.45):
        self.bg_noise, self.bg_noise_stft = self._prepare_noise_db(sample_rate)
        self.noise_prob = noise_prob

    def __call__(self, sample):
        x, y = sample
        if np.random.rand() > self.noise_prob:
            return [x, y]
        else:
            if len(x.shape) == 1:
                x += random.choice(self.bg_noise)
                return [x, y]
            else:
                x += random.choice(self.bg_noise_stft)
                return [x, y]

    def _prepare_noise_db(self, sample_rate):
        audio_transform = AudioAugmentation()
        noise_files = glob.glob(os.path.join(SHARED_DATA_FOLDER, 'noise_samples') + r"/*.wav")
        length = int(1 * sample_rate)
        bg_noise = []
        bg_noise_stft = []
        for file in noise_files:
            s, _ = librosa.load(file, sample_rate)
            if length < len(s):
                s = s[:length]
            elif length > len(s):
                s = np.pad(s, (0, length - len(s)), "constant")
            noise, _ = audio_transform([s, []])
            bg_noise.append(s)
            bg_noise_stft.append(noise)
        return bg_noise, bg_noise_stft


class ToMelSpectrogram(object):
    """Creates the mel spectrogram from the short time fourier transform of a file. The result is a 32x32 matrix."""
    def __init__(self, n_fft=2048, n_mels=32, sample_rate=16000):
        self.n_mels = n_mels
        self.sample_rate = sample_rate
        self.n_fft = n_fft

    def __call__(self, sample):
        x, y = sample
        if len(x.shape) == 1:
            s = librosa.feature.melspectrogram(x, sr=self.sample_rate, n_mels=self.n_mels)
            x = librosa.power_to_db(s, ref=np.max)
            return [x, y]
        else:
            mel_basis = librosa.filters.mel(self.sample_rate, self.n_fft, self.n_mels)
            s = np.dot(mel_basis, np.abs(x)**2.0)
            x = librosa.power_to_db(s, ref=np.max)
            return [x, y]


class ConvertToTensor(object):
    def __init__(self, key):
        self.convert = transforms.ToTensor()
        self.key = key

    def __call__(self, sample):
        x, y = sample
        if self.key == 'segmentation':
            return [self.convert(x), self.convert(y)]
        else:
            return [self.convert(x), y]


