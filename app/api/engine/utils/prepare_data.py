#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 10.01.2018 15:08 $
# by : Fischer $
# SVN : $
#

# --- imports -----------------------------------------------------------------
import os
import glob
import numpy as np
from sklearn.model_selection import train_test_split


def write_txt(data_path, file_name, data, label=None):
    """
    Write data file path and names to .txt file
    :param data_path: path to save too
    :param file_name: filename to save too
    :param data:      list of filenames
    :param label:     optional: list of labels
    :return:
    """
    with open(os.path.join(data_path, file_name), 'w') as f:
        if label is not None:
            for d, l in zip(data, label):
                f.write("{} {:d}\n".format(d, l))
        else:
            for d in data:
                f.write("{}\n".format(d))


def generate_txt(data_paths, labels=None, test_size=0.33):
    """
    Generates a .txt file that contains all file paths and in case of classification also labels for a dataset
    Additionally splits the data into training and test set (using random selection and shuffling)
    :param data_paths:  data paths to parse (n for n classes/labels)
    :param labels:      if given set label (int) to all files
    :param test_size:   percentage of data used for testing
    :return:
    """
    X_train, X_test, Y_train, Y_test = [], [], [], []
    for i, dp in enumerate(data_paths):
        file_names = glob.glob(os.path.join(dp, '*.*'))

        # store only parent folder name and file name in list
        reduced_file_names = [os.path.join(os.path.basename(os.path.dirname(fn)), os.path.basename(fn)) for fn in
                              file_names]

        x_tr, x_te = train_test_split(reduced_file_names, test_size=test_size)
        X_train.extend(x_tr)
        X_test.extend(x_te)

        if labels:
            y_tr = [labels[i]] * len(x_tr)
            y_te = [labels[i]] * len(x_te)
            Y_train.extend(y_tr)
            Y_test.extend(y_te)

    if labels:
        write_txt(data_path, 'training.txt', X_train, Y_train)
        write_txt(data_path, 'testing.txt', X_test, Y_test)
    else:
        write_txt(data_path, 'training.txt', X_train)
        write_txt(data_path, 'testing.txt', X_test)


if __name__ == '__main__':

    # example to generate training.txt and testing.txt for files stored in two separate folders used for classification
    data_path = r'D:\DataSets\FraudDetection\face_copypaste'
    data_A_path = os.path.join(data_path, 'OK')
    data_B_path = os.path.join(data_path, 'FACE_COPYPASTE')

    generate_txt([data_A_path, data_B_path], labels=[0, 1])
