#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 25.01.2018 15:23 $
# by : Shepeleva $
# SVN : $
#

# --- imports -----------------------------------------------------------------

import os
import time


def log_loss_accuracy(accuracy, accuracy_type, task_type, num_classes):
    if isinstance(accuracy_type, list) and (len(accuracy_type) >= 2):
        acc_str = ''
        for k in range(len(accuracy_type)):
            acc_str += '{:s} '.format(accuracy_type[k])
            for i in range(num_classes):
                acc_str += 'lbl_{:d}: {:.3f} '.format(i, accuracy[k][i])
    else:
        if isinstance(accuracy_type, list):
            accuracy_type = accuracy_type[0]
        acc_str = '{:s}'.format(accuracy_type)
        if task_type == 'classification':
            acc_str += 'lbl: {:.3f} '.format(accuracy)
        elif task_type == 'segmentation':
            acc_str += 'lbl: {:.3f} '.format(accuracy)
            # for i in range(num_classes):
            #     acc_str += 'lbl_{:d}: {:.3f} '.format(i, accuracy[i])

    print(acc_str)
    # return acc_str


def create_ckpt_data(checkpoint_dir, network_type, event_time):
    file_path = os.path.join(os.path.dirname(os.path.abspath('main.py')), checkpoint_dir, network_type,
                             time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime(event_time)))
    if not os.path.exists(file_path):
        # create directory
        os.makedirs(file_path)
        
    return "{}/model.ckpt".format(file_path)


def create_ckpt_data_pytorch(checkpoint_dir):
    return "{}/model.pth".format(checkpoint_dir)

def create_ckpt_data_onnx(checkpoint_dir):
    return "{}/onnx_model.onnx".format(checkpoint_dir)
