#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 3/29/2020 11:21 PM $
# by : clu $
# SVN : $
#

import h5py
import time
import timeit
import numpy as np
from tqdm import tqdm
# import resource

import random 
import torch.nn as nn
# from utils.mAP.compute_mAP import mAP_metric
#
# from utils.livelossplot.generic_plot import PlotLosses

import torch

from torch.optim.lr_scheduler import ReduceLROnPlateau, StepLR
from torch.utils.tensorboard import SummaryWriter

from network.NetRunner import NetRunner
from utils.BatchIterator import BatchIterator
from utils.logging import log_loss_accuracy, create_ckpt_data_pytorch, create_ckpt_data_onnx

from datetime import datetime

from utils.metric_pytorch import *
from torchvision import transforms

#FIXME hack cm tensorboard
import matplotlib.pyplot as plt
import itertools

import datetime as dt

from torch.autograd import Variable

SHARED_DATA_FOLDER = "/opt/www/shared_data/"

#FIXME hack cm tensorboard
def plot_confusion_matrix(cm):
    """
    Returns a matplotlib figure containing the plotted confusion matrix.
    
    Args:
       cm (array, shape = [n, n]): a confusion matrix of integer classes
       class_names (array, shape = [n]): String names of the integer classes
    """
    
    figure = plt.figure(figsize=(8, 8))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title("Confusion matrix")
    plt.colorbar()
    tick_marks = np.arange(cm.shape[0])
    plt.xticks(tick_marks, range(cm.shape[0]), rotation=45)
    plt.yticks(tick_marks, range(cm.shape[0]))
    
    # Normalize the confusion matrix.
    cm = cm.numpy()
    cm = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=3)
    
    # Use white text if squares are dark; otherwise black.
    threshold = cm.max() / 2.
    
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        color = "white" if cm[i, j] > threshold else "black"
        plt.text(j, i, cm[i, j], horizontalalignment="center", color=color)
        
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return figure

class TrainRunner(NetRunner):
    #FIXME hack confusion matrix
    def confusion_matrix(self, preds, labels, conf_matrix):
        preds = torch.argmax(preds, 1)
        labels = torch.argmax(labels, 1)
        for p, t in zip(preds, labels):
            conf_matrix[t, p] += 1


    def __init__(self, config, alg, dataset, preprocessing_pipeline_dict):
        super().__init__(config, alg, dataset, preprocessing_pipeline_dict)
        if self.cross_val > 2:
            self._iter = self.cross_val
        else:
            self._iter = 1

        print("training initialized")
        self.initialize_training()

        self.event_time = time.time()
        self.model2save = self.network

        self.model_file_path = create_ckpt_data_pytorch(self.pymodel_storage)
        self.model_onnx_path = create_ckpt_data_onnx(self.onnxmodel_storage)

        self.writer_train = SummaryWriter(self.ckpnt_path+"_train")
        self.writer_valid = SummaryWriter(self.ckpnt_path+"_valid")
        self.image_logger_train = Image_logger(tbx_writer=self.writer_train, tag='TRAINING/training/img', img_size=self.img_size)
        self.image_logger_valid = Image_logger(tbx_writer=self.writer_valid, tag='VALIDATION/training/img', img_size=self.img_size)

        self.best_params = {'epoch': 0,
                            'lr': 0,
                            'train_loss': 0,
                            'train_accur': 0,
                            'valid_loss': 0,
                            'valid_accur': 0}

        self.input_names = ["input_data"]
        self.output_names = ["output_data"]

        #FIXME hack confusion matrix
        self.num_classes = config.num_classes
        self.conf_matrix = torch.zeros(config.num_classes, config.num_classes)

    def start_training(self):
        if self.training_mode == 'full':
            status = self.start_full_training()
        elif self.training_mode == 'fast':
            status = self.start_fast_training()
        elif self.training_mode == 'validation':
            status = self.start_validation()
        else:
            raise ValueError("No such training mode exists. 'full' or 'fast' supported only")

        return status

    def start_full_training(self):
        h5_file = h5py.File(self.storage_location, 'r')

        from plugins.utils.pipeline import Pipeline

        test_size_pipeline = Pipeline(self.preprocessing_pipeline_instance)
        batch_transforms_composed = transforms.Compose(test_size_pipeline.get_transforms('batch_transforms'))


        self.actual_input_size = (batch_transforms_composed(h5_file['X_data'][0])).shape
        print ("GUI input size: ", self.img_size)
        print ("Actual input size: ", self.actual_input_size)
        
        iterators = 0
        for split in reversed(range(len(self.slice_split))):
            if self._iter <= iterators:
                break
            splt_slice = self.slice_split.copy()
            valid_split = splt_slice[split]
            train_split = []
            splt_slice.remove(valid_split)
            for i in splt_slice:
                train_split.extend(i)

            print("Cross validation split {} of {}".format(str(len(self.slice_split) - split), str(len(self.slice_split)-1)))

            self.thresh_inc = [self.conf_threshold/self.num_epochs, self.nms_threshold/self.num_epochs]

            prev_loss = np.inf
            ref_iter_count = 0

            rp = ReduceLROnPlateau(self.train_op, factor=self.lr_decay, patience=self.ref_patience)
            start = timeit.default_timer()
            for epoch in range(1, self.num_epochs + 1):
                self.image_logger_train.clear()
                self.image_logger_valid.clear()
                #FIXME hack confusion matrix
                if self.task_type == 'classification':
                  self.conf_matrix = torch.zeros(self.num_classes, self.num_classes)
                  
                train_aver_loss, epoch_accur_train, epoch_duration_train = self._training_step(epoch, h5_file, train_split)
                
                if np.isnan(train_aver_loss):
                  return "NaN"
                  
             #   print(self.conf_matrix)
                if self.task_type == 'classification':
                  self.writer_train.add_figure('TRAINING_CM', plot_confusion_matrix(self.conf_matrix), epoch)
                  self.conf_matrix = torch.zeros(self.num_classes, self.num_classes)
                  
                valid_aver_loss, epoch_accur_valid, epoch_duration_valid = self._validation_step(epoch, h5_file, valid_split)
                
              #  print(self.conf_matrix)
                if self.task_type == 'classification':
                  self.writer_valid.add_figure('VALIDATION_CM', plot_confusion_matrix(self.conf_matrix), epoch)

                self.writer_train.add_scalar('TRAINING/loss', train_aver_loss, epoch)
                self.writer_valid.add_scalar('TRAINING/loss', valid_aver_loss, epoch)
                self.writer_train.add_scalar('TRAINING/accuracy', epoch_accur_train, epoch)
                self.writer_valid.add_scalar('TRAINING/accuracy', epoch_accur_valid, epoch)
                self.writer_train.add_scalar('TRAINING/Learning_rate', self.train_op.param_groups[0]['lr'], epoch)

                prev_lr = self.train_op.param_groups[0]['lr']
                rp.step(valid_aver_loss)
                curr_lr = self.train_op.param_groups[0]['lr']
                print("Prev lr: {} Curr lr: {}".format(prev_lr, curr_lr))

                print('\nRESULTS: epoch {:d} train loss = {:.3f}, train accuracy : {:.3f} ({:.2f} sec) || '
                'valid loss = {:.3f}, valid accuracy : {:.3f} ({:.2f} sec)'
                .format(epoch, train_aver_loss, epoch_accur_train, epoch_duration_train, valid_aver_loss,
                        epoch_accur_valid, epoch_duration_valid))
                if (epoch) % 15 == 0 and epoch>0:
                  print ("Exporting temporary ONNX. Epoch {}".format(epoch))
                  self._save_onnx_model(tag = "_epoch{}".format(epoch))
                if curr_lr < prev_lr:
                    ref_iter_count += 1
                    if ref_iter_count == self.ref_steps+1:
                        print("Early stopping")
                        if (prev_loss - valid_aver_loss) > 0.001:
                            self._save_torch_model(epoch, curr_lr, train_aver_loss, epoch_accur_train, valid_aver_loss,
                                                   epoch_accur_valid)
                            self._save_onnx_model(tag = "early_stop_epoch{}".format(epoch+1))
                        else:
                            print("This model has worse performace than the previouse one.")
                            print('Loading state: epoch {:d} train loss = {:.3f}, train accuracy : {:.3f} || valid loss = {:.3f}, valid accuracy : {:.3f}'.format(self.best_params['epoch'], self.best_params['train_loss'], self.best_params['train_accur'], self.best_params['valid_loss'], self.best_params['valid_accur']))
                            self.writer_valid.close()
                            self.writer_train.close()
                            self.model2save.load_state_dict(torch.load(self.model_file_path))
                            self._save_onnx_model()
                            finish = timeit.default_timer()
                            self.save_to_db(finish-start)
                        status = "Finished. Early stopping"
                        return status
                else:
                    if (prev_loss - valid_aver_loss) > 0.001:
                        prev_loss = valid_aver_loss
                        self._save_torch_model(epoch, curr_lr, train_aver_loss, epoch_accur_train, valid_aver_loss,
                                               epoch_accur_valid)

            if (prev_loss - valid_aver_loss) > 0.001:
                self.writer_valid.close()
                self.writer_train.close()
                self._save_torch_model(epoch, curr_lr, train_aver_loss, epoch_accur_train, valid_aver_loss,
                                       epoch_accur_valid)
                self._save_onnx_model()
                finish = timeit.default_timer()
                self.save_to_db(finish-start)
            else:
                print("This model has worse performace than the previouse one. All states will be loaded from the lastr best model")
                print('Loading state: epoch {:d} train loss = {:.3f}, train accuracy : {:.3f} || valid loss = {:.3f}, valid accuracy : {:.3f}'.format(
                        self.best_params['epoch'], self.best_params['train_loss'], self.best_params['train_accur'],
                        self.best_params['valid_loss'], self.best_params['valid_accur']))
                self.writer_valid.close()
                self.writer_train.close()
                self.model2save.load_state_dict(torch.load(self.model_file_path))
                self._save_onnx_model()
                finish = timeit.default_timer()
                self.save_to_db(finish-start)

            status = "Finished. Fully trained"
            iterators += 1
        return status

    def start_validation(self):
        h5_file = h5py.File(self.storage_location, 'r')

        from plugins.utils.pipeline import Pipeline

        test_size_pipeline = Pipeline(self.preprocessing_pipeline_instance)
        batch_transforms_composed = transforms.Compose(test_size_pipeline.get_transforms('batch_transforms'))

        self.actual_input_size = (batch_transforms_composed(h5_file['X_data'][0])).shape
        print ("GUI input size: ", self.img_size)
        print ("Actual input size: ", self.actual_input_size)
        
        iterators = 0
        for split in reversed(range(len(self.slice_split))):
            if self._iter <= iterators:
                break
            splt_slice = self.slice_split.copy()
            valid_split = splt_slice[split]
            train_split = []
            splt_slice.remove(valid_split)
            for i in splt_slice:
                train_split.extend(i)

            print("Cross validation split {} of {}".format(str(len(self.slice_split) - split), str(len(self.slice_split)-1)))

            prev_loss = np.inf
            ref_iter_count = 0
            epoch = 0
            
            start = timeit.default_timer()
            valid_aver_loss, epoch_accur_valid, epoch_duration_valid = self._validation_step(epoch, h5_file, valid_split+train_split)

            self.writer_valid.add_scalar('TRAINING/loss', valid_aver_loss, epoch)
            self.writer_valid.add_scalar('TRAINING/accuracy', epoch_accur_valid, epoch)

            print('\nRESULTS: epoch {:d} valid loss = {:.3f}, valid accuracy : {:.3f} ({:.2f} sec)'
            .format(epoch, valid_aver_loss,  epoch_accur_valid, epoch_duration_valid))
            
            self.writer_valid.close()
            #self.model2save.load_state_dict(torch.load(self.model_file_path))
            self._save_onnx_model(tag = "_post_validation")
            finish = timeit.default_timer()
            self.save_to_db(finish-start)

            status = "Finished. Validated"
            iterators += 1
        return status

    def start_fast_training(self):
        raise NotImplementedError


    def _training_step(self, epoch, h5_file, train_split):
        train_generator = BatchIterator(h5_file, train_split, self.img_size, self.num_classes, self.batch_size,
                                        self.task_type, self.device, 'train', self.mean, self.var,
                                        self.preprocessing_pipeline_instance, self.augment_dict)
        train_lim = train_generator.get_max_lim()

        epoch_loss_train = 0
        epoch_accur_train = 0
        self.network.train()
        start = timeit.default_timer()
        #print (self.network.state_dict())
        j=0
        for i in tqdm(train_generator, total=train_lim, unit=' steps', desc='Epoch {:d} train'.format(epoch)):
            ff = list(i)

            x = torch.stack([f[0] for f in ff]).to(self.device, dtype=torch.float)
            self.train_op.zero_grad()
            outputs = self.network(x)
            

            if self.task_type == 'classification':
                y = torch.from_numpy(np.array([f[1] for f in ff])).to(self.device, dtype=torch.float)
                out = outputs.detach().clone()
                loss = self.network.return_loss(outputs, y).to(self.device)
                        
                if not self.image_logger_train.done_for_epoch:
                   
                  ind = random.randint(0,len(x)-1)
                  timage= x[ind].cpu()
                  pred = classification_class(out)[ind]
                  self.writer_train.add_image("TRAINING/training/img_class_{}".format(pred),timage,epoch)
                  self.image_logger_train.done_for_epoch = True
            elif self.task_type == 'segmentation':
                y = torch.tensor([np.uint8(f[1]) for f in ff]).to(self.device, dtype=torch.float)
                out = outputs.detach().clone()
                loss = self.network.return_loss(outputs, y,  smooth=1e-7).to(self.device)
                
#                print (x.shape)
#                print (y.shape)
#                print (out.shape)
                prd = np.round(nn.Sigmoid()(out.cpu()))
                
                if not self.image_logger_train.done_for_epoch:
#                  print (x[0][1:2].cpu())
#                  print (y[0].cpu())
#                  print (prd[0].cpu())
                  timage= np.concatenate((x[0][1:2].cpu(), y[0].cpu(), prd[0].cpu()),axis=2)
                  self.writer_train.add_image("TRAINING/training/img_{}".format(j),timage,epoch)
                  j+=1
                  if j==10:
                    self.image_logger_train.done_for_epoch = True
                
            elif self.task_type == 'detection':
            
                y = [torch.from_numpy(np.array(f[1])).float().to(self.device) for f in ff]
                
                loss = self.network.return_loss(outputs, y, anchors=self.anchors, num_classes=self.num_classes, iou_thres=self.iou_thres).to(self.device)
                
                
                
                
            loss.backward()
            self.train_op.step()
            torch.cuda.empty_cache()

            epoch_loss_train += loss.detach().item()

            #FIXME: hack add confusion matrix
            if self.task_type == 'classification':
                self.confusion_matrix(out, y, self.conf_matrix)

            if self.task_type == 'detection':
            
                out = predictions(outputs, anchors=self.anchors, image_size=self.img_size[0], conf_threshold=self.conf_threshold, nms_threshold=self.nms_threshold)
                
                train_step_accuracy = self.network.return_accuracy(out, y, iouThreshold=self.iou_thres)
                                                                   
                self.image_logger_train.draw_results(out, x, epoch)
                if j%int(train_lim/20):
                  self.image_logger_train.clear()
                j+=1
                epoch_accur_train += train_step_accuracy#.detach().item()
            else:
                train_step_accuracy = self.network.return_accuracy(outputs, y, smooth=1e-7).to(self.device)
                epoch_accur_train += train_step_accuracy.detach().item()

        #print (self.network.state_dict())
        epoch_accur_train = epoch_accur_train / train_lim
        epoch_loss_train = epoch_loss_train / train_lim

        finish = timeit.default_timer()
        return epoch_loss_train, epoch_accur_train, finish-start

    def _validation_step(self, epoch, h5_file, valid_split):
        with torch.no_grad():
            valid_generator = BatchIterator(h5_file, valid_split, self.img_size, self.num_classes, self.batch_size,
                                            self.task_type, self.device, 'valid', self.mean, self.var,
                                            self.preprocessing_pipeline_instance, self.augment_dict)
            valid_lim = valid_generator.get_max_lim()
            epoch_loss_valid = 0
            epoch_accur_valid = 0
            self.network.eval()
            start = timeit.default_timer()
            j=0
            for i in tqdm(valid_generator, total=valid_lim, unit=' steps', desc='Epoch {:d} valid'.format(epoch)):
                ff = list(i)
                x = torch.stack([f[0] for f in ff]).to(self.device, dtype=torch.float)
                with torch.no_grad():

                    outputs = self.network(x)

                    if self.task_type == 'classification':
                        y = torch.from_numpy(np.array([f[1] for f in ff])).to(self.device, dtype=torch.float)
                        out = outputs.detach().clone()
                        loss = self.network.return_loss(outputs, y).to(self.device) 
                        
                        if not self.image_logger_valid.done_for_epoch:
                          ind = random.randint(0,len(x)-1)
                          timage= x[ind].cpu()
                          pred = classification_class(out)[ind]
                          self.writer_valid.add_image("VALIDATION/training/img_class_{}".format(pred),timage,epoch)
                          self.image_logger_valid.done_for_epoch = True
                    elif self.task_type == 'segmentation':
                        y = torch.tensor([np.uint8(f[1]) for f in ff]).to(self.device, dtype=torch.float)
                        out = outputs.detach().clone()
                        loss = self.network.return_loss(outputs, y, smooth=1e-7).to(self.device)
                
                        prd = np.round(nn.Sigmoid()(out.cpu()))
                        if not self.image_logger_valid.done_for_epoch:
                          timage= np.concatenate((x[0][1:2].cpu(), y[0].cpu(), prd[0].cpu()),axis=2)
                          self.writer_valid.add_image("VALIDATION/training/img_{}".format(j),timage,epoch)
                          j+=1
                          if j==10:
                            self.image_logger_valid.done_for_epoch = True
                    elif self.task_type == 'detection':
                    
                        y = [torch.from_numpy(np.array(f[1])).float().to(self.device) for f in ff]
                        
                        loss = self.network.return_loss(outputs, y, anchors=self.anchors, num_classes=self.num_classes, iou_thres=self.iou_thres).to(self.device)
                        
                            
                            
                            
                        
                    epoch_loss_valid += loss.detach().item()

                    #FIXME: hack add confusion matrix
                    if self.task_type == 'classification':
                        self.confusion_matrix(out, y, self.conf_matrix)

                    if self.task_type == 'detection':
                        out = predictions(outputs, anchors=self.anchors, image_size=self.img_size[0], conf_threshold=self.conf_threshold, nms_threshold=self.nms_threshold)
                        
#                        with open(self.model_onnx_path+".txt",'w') as f:
#                          f.write ("x=")
#                          try:
#                            f.write (str(x.tolist()))
#                          except:
#                            f.write (str(x))
#                            
#                          f.write ("outputs=")
#                          try:
#                            f.write (str(outputs.tolist()))
#                          except:
#                            f.write (str(outputs))
#                          
#                          f.write ("out=")
#                          try:
#                            f.write (str(out.tolist()))
#                          except:
#                            f.write (str(out))
#                        
#                        exit(0)
                        valid_step_accuracy = self.network.return_accuracy(out, y, iouThreshold=self.iou_thres)
                        
                        
                        
                        draw_GT=False
                        
                        if draw_GT:
                          out=[]
                          for im in y:
                            objs = []
                            for o in im:
                              objs.append([o[0],
                                         o[1],
                                         o[2] + o[0],
                                         o[3] + o[1],
                                         1,
                                         o[4]])
                            out.append(objs)
                            
                        
                        self.image_logger_valid.draw_results(out, x, epoch)
                        if j%int(valid_lim/20):
                          self.image_logger_valid.clear()
                        j+=1
                        epoch_accur_valid += valid_step_accuracy#.detach().item()

                    else:
                        valid_step_accuracy = self.network.return_accuracy(outputs, y, smooth=1e-7).to(self.device).detach().item()
                        epoch_accur_valid += valid_step_accuracy


            epoch_accur_valid = epoch_accur_valid / valid_lim
            epoch_loss_valid = epoch_loss_valid / valid_lim

        finish = timeit.default_timer()
        return epoch_loss_valid, epoch_accur_valid, finish - start

    def _save_torch_model(self, epoch, curr_lr, train_aver_loss, epoch_accur_train, valid_aver_loss, epoch_accur_valid):
        print("Current model is the best model. Saving")
        self.best_params['epoch'] = epoch
        self.best_params['lr'] = curr_lr
        self.best_params['train_loss'] = train_aver_loss
        self.best_params['train_accur'] = epoch_accur_train
        self.best_params['valid_loss'] = valid_aver_loss
        self.best_params['valid_accur'] = epoch_accur_valid
        torch.save(self.model2save.state_dict(), self.model_file_path)

    def _save_onnx_model(self, tag = ""):
        
        model_onnx_path = self.model_onnx_path.replace(".onnx","{}.onnx".format(tag))
        
        print("Exporting model to ONNX")
        dummy_input = Variable(torch.randn(1, *self.actual_input_size, device=self.device))
        torch.onnx.export(self.model2save, dummy_input, model_onnx_path, verbose=False, opset_version=9,
                          input_names=self.input_names, output_names=self.output_names)

    def save_to_db(self, exec_time):
        self.alg_handle.onnx_trained = self.model_onnx_path.replace(SHARED_DATA_FOLDER,"")
        self.alg_handle.pytorch_trained = self.model_file_path.replace(SHARED_DATA_FOLDER,"")

        self.alg_handle.training_loss = self.best_params['train_loss']
        self.alg_handle.training_accuracy = self.best_params['train_accur']
        self.alg_handle.validation_loss = self.best_params['valid_loss']
        self.alg_handle.validation_accuracy = self.best_params['valid_accur']


        self.alg_handle.training_log = None
        self.alg_handle.execution_time = exec_time
        self.alg_handle.save()
