#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 3/29/2020 9:22 PM $
# by : clu $
# SVN : $
#
import os
import importlib
import torch
# from db.mongo import MongoConnector

from utils.DataParser import DataParser
# from utils.onnx2pytorch.Generic_layer import Generic_layer
# from utils.onnx2pytorch.Initializer import Initializer
# from utils.onnx2pytorch.Pytorch_script_gen import Pytorch_script_gen
from network.wrappers import ConvNet, VGG19, UNet, YoloV3Tiny
# # from models.models import *
# from utils.onnx2pytorch.onnx2pytorch import onnxextract
from torchvision import transforms
from utils.onnxparser.onnxparser import onnxextract

import time
import onnx
import platform
if platform.node() == 'AMURSK':
    SHARED_DATA_FOLDER = r"E:\SCCH_PROJECTS\ALOHA\trunk\TE_v2.0\TrainingEngine_v2_no_docker\data_folder"
else:
    SHARED_DATA_FOLDER = "/opt/www/shared_data/"

class NetRunner:

    def __init__(self, config, alg, dataset, preprocessing_pipeline_dict):
        print("net runner initializer")
        if config.project is None:
            raise ValueError("No project assigned to the task")

        self.alg_handle = alg
        self.project_id = alg.project
        self.experiment_id = alg.get_id()
        self.model = alg.onnx

        self.storage_location = os.path.join(SHARED_DATA_FOLDER,  dataset.path)
        self.num_classes = dataset.num_classes
        self.mean = dataset.mean
        self.var = dataset.var
        self.slice_split = dataset.slice_split

        self.task_type = config.task_type
        self.num_epochs = config.num_epochs
        
        print ("GUI input size: ", config.image_dimensions)
        dims = []
        for i, d in enumerate(config.image_dimensions):
          if d>0:
            dims.append(d)
          else:            
            print ("Dimension {} is set to zero. Removing it.".format(i))
            
        self.img_size = dims
        print ("Reshaped input size: ", self.img_size)
        
        try:
          self.discard_params = not (config.algo_parameters['load_params'])
        except:
          self.discard_params = True
        
        try:
          self.yolo_from_classifier = config.algo_parameters['yolo_from_classifier']
        except:
          self.yolo_from_classifier = False
        
        self.cross_val = config.cross_val

        self.sample_rate = config.sample_rate
        self.n_mels = config.n_mels
        self.bg_noise_pcnt = config.bg_noise_pcnt

        # plugins
        print("preprocessing pipeline dict", preprocessing_pipeline_dict)
        self.preprocessing_pipeline_instance = preprocessing_pipeline_dict

        self.augment_dict = config.augment_dict
        self.training_mode = config.training_mode
        self.num_filters = config.num_filters

        self.hp_search = False
        self.lr = config.lr
        self.lr_decay = config.lr_decay
        self.ref_steps = config.ref_steps
        self.ref_patience = config.ref_patience
        self.batch_size = config.batch_size
        self.loss = config.loss
        self.accuracy = config.accuracy
        self.optimizer = config.optimizer
        self.nonlin = config.nonlin

        self.anchors = config.anchors
        self.divider = config.divider
        self.giou_loss_gain = config.giou_loss_gain
        self.cls_loss_gain = config.cls_loss_gain
        self.obj_loss_gain = config.obj_loss_gain
       # self.iou_t = config.iou_t
        self.iou_thres = config.iou_thres
        #self.conf_thres = config.conf_thres
        
        self.conf_threshold = config.conf_thres
        self.nms_threshold = 0.3
        

        self._path_init()
        
        if self.task_type == 'classification':
          self.cnn_out_channels = self.num_classes
        elif self.task_type == 'segmentation':
          self.cnn_out_channels = self.num_classes #FIXME: is that correct?
        elif self.task_type == 'detection':
          self.cnn_out_channels = (self.num_classes+5)*len(self.anchors)

        # if config.hyperparam_search == True:
        #     self.hp_search = True
        #     raise NotImplementedError
        # else:
        #     self.hp_search = False
        #     self.lr = config.lr
        #     self.lr_decay = config.lr_decay
        #     self.ref_steps = config.ref_steps
        #     self.ref_patience = config.ref_patience
        #     self.batch_size = config.batch_size
        #     self.loss = config.loss
        #     self.accuracy = config.accuracy
        #     self.optimizer = config.optimizer
        #     self.nonlin = config.nonlin
        #
        # self._path_init()


    def _path_init(self):
        self.experiment_path = os.path.join(SHARED_DATA_FOLDER, 'experiments', "prj_" + str(self.project_id),
                                            "alg_" + str(self.experiment_id))
        if not os.path.isdir(self.experiment_path):
            os.makedirs(self.experiment_path)
        self.ckpnt_path = os.path.join(self.experiment_path, "ckpnt_logs")
        if not os.path.isdir(self.ckpnt_path):
            os.mkdir(self.ckpnt_path)
        self.onnxmodel_storage = os.path.join(self.experiment_path, "onnx_storage")
        if not os.path.isdir(self.onnxmodel_storage):
            os.mkdir(self.onnxmodel_storage)
        self.pymodel_storage = os.path.join(self.experiment_path, "pytorch_storage")
        if not os.path.isdir(self.pymodel_storage):
            os.mkdir(self.pymodel_storage)


    def initialize_training(self):
        cuda_en = torch.cuda.is_available()
        if cuda_en:
            if torch.cuda.device_count()>1:
                gpu_id = int(os.environ.get('GPU_ID', 0))
            else:
                gpu_id=0
            print("CUDA device found. Training mapped to GPU")
            print("This machine has %d GPU(s) and the GPU with id %d is going to be used" %(torch.cuda.device_count(), gpu_id))
            print("\t Check the environment variable GPU_ID to change the GPU's id")
            self.device = torch.device('cuda:%d'%gpu_id)
        else:
            print("CUDA device NOT found. Training mapped to CPU")
            self.device = torch.device('cpu')

        if self.model.endswith('.onnx'):
            self.network_type = os.path.split(self.model)[-1][:-5]
            print("loading onnx model")
            self.network = self._load_model().to(self.device)
        else:
            self.network_type = self.model
            print("loading predefined model")
            self.network = self._pick_model().to(self.device)
        self.train_op = self.network.return_optimizer(self.network.parameters(), self.lr)
        print("train_op")

    def _pick_model(self):
        if self.model == 'ConvNet':
            return ConvNet.ConvNet(self.device, self.network_type, self.loss, self.accuracy, self.lr, self.img_size[-1],
                                   self.optimizer, self.num_classes, self.nonlin, self.num_filters)
        elif self.model == 'VGG19':
            return VGG19.VGG19(self.device, self.network_type, self.loss, self.accuracy, self.lr, self.img_size[-1],
                               self.optimizer, self.num_classes, self.nonlin, self.num_filters)
        elif self.model == 'UNet':
            return UNet.UNet(self.device, self.network_type, self.loss, self.accuracy, self.lr, self.img_size[-1],
                             self.optimizer, self.num_classes, self.nonlin, self.num_filters)
        elif self.model == 'YoloV3Tiny':
            return YoloV3Tiny.YoloV3Tiny(self.device, self.network_type, self.loss, self.accuracy, self.lr, self.img_size[-1],
                             self.optimizer, self.num_classes, self.img_size, self.nonlin, self.num_filters)

        else:
            raise ValueError('Unexpected model type %s' % self.network_type)

    def customize_class(self, path):
      with open(path, "r") as net:
        lines = net.readlines()
      with open(path, "w") as net:
        for line in lines:
          if line.find("class Net(nn.Module):")>=0:
            net.write(line.replace("class Net(nn.Module):", "from network.wrappers.NetworkBase import NetworkBase\nclass Net(NetworkBase, nn.Module):"))          
          elif line.find("def __init__(self):")>=0:
            net.write(line.replace("def __init__(self):", "def __init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer, out_channels, img_size, nonlin=None, num_filters=None, trainable_layers=None):"))
          elif line.find("super(Net, self).__init__()")>=0:
            net.write(line.replace("super(Net, self).__init__()", "NetworkBase.__init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer, out_channels, img_size, nonlin, num_filters, trainable_layers)\n        nn.Module.__init__(self)"))
          else:
            net.write(line)

    def _load_model(self):
        # path_to_wrapper = r'/opt/www/api/engine/network/wrappers/generated_model'
        path_to_te_wrapper = os.path.join(os.path.join(SHARED_DATA_FOLDER, "experiments/prj_{}/alg_{}/net".format(str(self.project_id), str(self.experiment_id))), 'te')
        if not os.path.exists(path_to_te_wrapper):
            os.makedirs(path_to_te_wrapper)

        path_to_se_wrapper = os.path.join(os.path.join(SHARED_DATA_FOLDER, 'generated_model'), 'se')
        if not os.path.exists(path_to_se_wrapper):
            os.makedirs(path_to_se_wrapper)

        parsed_te_onnx = os.path.join(path_to_te_wrapper, 'Net.py')
        
        
        onnxextract(model_file         = os.path.join(SHARED_DATA_FOLDER, self.model),
                    path               = path_to_te_wrapper,
                    class_elements     = self.cnn_out_channels,
                    batch_norm_folding = False,
                    discard_params     = self.discard_params,
                    yolo_like          = self.yolo_from_classifier,
                    target             = "pytorch")

        self.customize_class(parsed_te_onnx)
        
        
        mod_name = os.path.basename(parsed_te_onnx).split('.')[0]
        
        localimport=path_to_te_wrapper.replace(SHARED_DATA_FOLDER,"")
        localimport= "shared_data." + localimport.replace("/",".") + "."
        
        print(mod_name)
        print("importing module", localimport+ mod_name)
        net = importlib.import_module(localimport+ mod_name)
        print("module imported")
        return net.Net(self.device, self.network_type, self.loss, self.accuracy, self.lr, self.img_size[-1],
                             self.optimizer, self.num_classes, self.img_size)
