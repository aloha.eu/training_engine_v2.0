#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 10/30/2018 2:28 PM $ 
# by : shepeleva $ 
# SVN  $
#

# --- imports -----------------------------------------------------------------

import torch.nn as nn
import torch.nn.functional as F

from network.wrappers.NetworkBase import NetworkBase


class ConvNet(NetworkBase, nn.Module):

    def __init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer, out_channels, nonlin, num_filters):
        NetworkBase.__init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer, out_channels=out_channels, nonlin=nonlin, num_filters=num_filters)
        nn.Module.__init__(self)

        if self.num_filters == None:
            self.num_filters = 32

        self.conv1 = nn.Conv2d(in_channels, self.num_filters, 3, 1)
        self.conv2 = nn.Conv2d(self.num_filters, self.num_filters*2, 3, 1)
        self.dropout1 = nn.Dropout2d(0.25)
        self.dropout2 = nn.Dropout2d(0.5)
        self.fc1 = nn.Linear(self.num_filters*288, self.num_filters*8)
        self.fc2 = nn.Linear(self.num_filters*8, out_channels)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = F.relu(x)
        x = F.max_pool2d(x, 2)
        x = self.dropout1(x)
        x = x.view(x.size(0), -1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.dropout2(x)
        x = self.fc2(x)

        return x
