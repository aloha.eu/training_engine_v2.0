#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 10/30/2018 2:46 PM $ 
# by : shepeleva $ 
# SVN  $
#

# --- imports -----------------------------------------------------------------
from utils.metric_pytorch import *
from utils.mAP.compute_mAP import *

import torch
import torch.nn as nn


class NetworkBase:
    def __init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer, out_channels=None, img_size=None, nonlin=None, num_filters=None,
                 trainable_layers=None):

        """
        :param network_type:
        :param loss:
        :param accuracy:
        :param lr:
        :param training:
        :param optimizer:
        :param nonlin:
        :param upconv:
        :param num_filters:
        :param num_classes:
        :param dropout:
        :param num_steps:
        :param trainable_layers:
        """

        self.learning_rate = lr
        self.optimizer    = optimizer
        self.network_type = network_type
        self.in_channels  = in_channels
        self.loss_f       = self._pick_loss_func(loss)
        self.optimizer_f  = self._pick_optimizer_func(optimizer)
        self.accuracy_f   = self._pick_accuracy_function(accuracy)
        self.out_channels = out_channels
        self.img_size     = img_size
        self.device       = device
        self.num_filters  = num_filters
        if nonlin:
            self.nonlin_f = self._pick_nonlin_func(nonlin)

        if out_channels:
            self.out_channels = out_channels

        if trainable_layers:
            self.trainable_layers = trainable_layers
        else:
            self.trainable_layers = 'all'

    @staticmethod
    def _pick_loss_func(key):
        """
        Select loss function
        :param key: loss function identifier
        :return:    loss function
        """
        if key == 'softmax':
            return softmax
        if key == 'sigmoid':
            return sigmoid
        if key == 'margin':
            return margin
        if key == 'detection_loss':
            return detection_loss
        if key == 'dice':
            return dice_loss
        if key == 'bce_dice':
            return bce_dice_loss
        else:
            raise ValueError('Unexpected metric function %s' % key)

    @staticmethod
    def _pick_optimizer_func(key):
        if key == 'adam':
            return torch.optim.Adam
        elif key == 'sgd':
            return torch.optim.SGD
        elif key == 'rmsprob':
            return NotImplementedError
        else:
            return NotImplementedError

    @staticmethod
    def _pick_nonlin_func(key):
        if key == 'elu':
            return nn.ELU
        elif key == 'relu':
            return nn.ReLU
        elif key == 'lrelu':
            return nn.LeakyReLU
        elif key == 'sigmoid':
            return nn.Sigmoid
        else:
            raise ValueError('Unexpected nonlinearity function %s' % key)

    @staticmethod
    def _pick_accuracy_function(key):
        if key == 'IoU':
            return IoU
        elif key == 'dice':
            return dice_Coefficient
        elif key == 'mse':
            return NotImplementedError
        elif key == 'percent':
            print('percent acc')
            return percentage
        elif key == 'mAP':
            return mAP_metric #yolo_accuracy
        else:
            raise ValueError('Unexpected metric function %s' % key)

    def _optimizer_function(self, params, learning_rate):
        return self.optimizer_f(params, learning_rate)

    def _loss_function(self, y_pred, y_true, **kwargs):
        return self.loss_f(y_pred, y_true, **kwargs)

    def _accuracy_function(self, y_pred, y_true, **kwargs):
        return self.accuracy_f(y_pred=y_pred, y_true=y_true, **kwargs)

    def return_accuracy(self, y_pred, y_true, **kwargs):
        """
        Returns the prediction accuracy
        :param y_pred:  prediction
        :param y_true:  ground truth
        :return:        accuracy
        """
        return self._accuracy_function(y_pred=y_pred, y_true=y_true, **kwargs)

    def return_loss(self, y_pred, y_true, **kwargs):
        """
        Returns the loss
        :param y_pred:  prediction
        :param y_true:  ground truth
        :return:        loss
        """
        return self._loss_function(y_pred, y_true,  **kwargs)

    def return_optimizer(self, params, learning_rate):
        """
        Returns the optimizer function
        :param global_step: current global step
        :return:            optimizer
        """
        return self._optimizer_function(params, learning_rate)


    @staticmethod
    def _conv_bn_layer(n_in, n_out, filter_size, stride, nonlin_f, padding):
        m = nn.Sequential(
            nn.Conv2d(n_in, n_out, filter_size, stride, padding),
            nn.BatchNorm2d(n_out),
            nonlin_f()
        )
        return m

    @staticmethod
    def _conv_layer(n_in, n_out, filter_size, stride, nonlin_f, padding):
        m = nn.Sequential(
            nn.Conv2d(n_in, n_out, filter_size, stride, padding),
            nonlin_f()
        )
        return m
