#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 4/16/2020 9:44 AM $
# by : clu $
# SVN : $
#

import torch
import torch.nn as nn
import torch.nn.functional as F

from utils.metric_pytorch import *

from network.wrappers.NetworkBase import NetworkBase


class YoloLayer(nn.Module):

    def __init__(self, num_anchors, num_classes):
        super(YoloLayer, self).__init__()
        self.num_anchors = num_anchors
        self.num_classes = num_classes

    def forward(self, x):
        num_samples = x.size(0)
        grid_size = x.size(2)

        prediction = (
            x.view(num_samples, self.num_anchors, self.num_classes + 5, grid_size, grid_size)
            .permute(0, 1, 3, 4, 2)
            .contiguous()
        )

        return prediction.view(num_samples, -1, self.num_classes + 5)


class Upsample(nn.Module):
    """ nn.Upsample is deprecated """

    def __init__(self, scale_factor=2, mode="nearest"):
        super(Upsample, self).__init__()
        self.scale_factor = scale_factor
        self.mode = mode

    def forward(self, x):
        x = F.interpolate(x, scale_factor=self.scale_factor, mode=self.mode)
        return x


class YoloV3Tiny(NetworkBase, nn.Module):
    def __init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer, out_channels, img_size, nonlin, num_filters):
        NetworkBase.__init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer,  out_channels = out_channels, img_size = img_size, nonlin = nonlin, num_filters=num_filters)
        nn.Module.__init__(self)

        if self.num_filters == None:
            self.num_filters = 16
        num_anchors = 3

        self.conv_1 = self._conv_bn_layer(n_in=in_channels, n_out=self.num_filters, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)
        self.maxpool_1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_2 = self._conv_bn_layer(n_in=self.num_filters, n_out=self.num_filters*2, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)
        self.maxpool_2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_3 = self._conv_bn_layer(n_in=self.num_filters*2, n_out=self.num_filters*4, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)
        self.maxpool_3 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_4 = self._conv_bn_layer(n_in=self.num_filters*4, n_out=self.num_filters*8, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)
        self.maxpool_4 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_5 = self._conv_bn_layer(n_in=self.num_filters*8, n_out=self.num_filters*16, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)
        self.maxpool_5 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_6 = self._conv_bn_layer(n_in=self.num_filters*16, n_out=self.num_filters*32, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)

        self.maxpool_6 = nn.MaxPool2d(kernel_size=2, stride=1)
        self.zero_pad = nn.ZeroPad2d(padding=(0,1,0,1))

        self.conv_7 = self._conv_bn_layer(n_in=self.num_filters*32, n_out=self.num_filters*64, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)
        self.conv_8 = self._conv_bn_layer(n_in=self.num_filters*64, n_out=self.num_filters*16, filter_size=1, stride=1, nonlin_f=self.nonlin_f, padding=0)
        self.conv_9 = self._conv_bn_layer(n_in=self.num_filters*16, n_out=self.num_filters*32, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)
        self.conv_10 = nn.Conv2d(self.num_filters*32, (self.out_channels+5)*3, 1, 1, 0)

        self.yoloout_1 = YoloLayer(num_anchors, self.out_channels)

        self.conv_11 = self._conv_bn_layer(n_in=self.num_filters*16, n_out=self.num_filters*8, filter_size=1, stride=1, nonlin_f=self.nonlin_f, padding=0)

        self.upsample = Upsample()

        self.conv_12 = self._conv_bn_layer(n_in=self.num_filters*16 + self.num_filters*8, n_out=self.num_filters*16, filter_size=3, stride=1, nonlin_f=self.nonlin_f, padding=1)
        self.conv_13 = nn.Conv2d(self.num_filters*16, (self.out_channels+5)*3, 1, 1, 0)

        self.yoloout_2 = YoloLayer(self.num_anchors, self.out_channels)


    def forward(self, X):
        x = self.conv_1(X)
        x = self.maxpool_1(x)
        x = self.conv_2(x)
        x = self.maxpool_2(x)
        x = self.conv_3(x)
        x = self.maxpool_3(x)
        x = self.conv_4(x)
        x = self.maxpool_4(x)
        x = out_1 = self.conv_5(x)
        x = self.maxpool_5(x)
        x = self.conv_6(x)
        x = self.maxpool_6(x)
        x = self.zero_pad(x)
        x = self.conv_7(x)
        x = out_2 = self.conv_8(x)
        x = self.conv_9(x)
        x = self.conv_10(x)
        yolo_pred_1 = self.yoloout_1(x)
        x = self.conv_11(out_2)
        x = self.upsample(x)
        x = torch.cat((x, out_1), 1)
        x = self.conv_12(x)
        x = self.conv_13(x)
        yolo_pred_2 = self.yoloout_2(x)


        yolo_outputs = torch.cat((yolo_pred_1, yolo_pred_2), 1).to(self.device)

        return yolo_outputs
