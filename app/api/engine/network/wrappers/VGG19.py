#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 3/21/2019 10:55 AM $ 
# by : Shepeleva $ 
# SVN  $
#

# --- imports -----------------------------------------------------------------

import torch.nn as nn

from network.wrappers.NetworkBase import NetworkBase


class VGG19(NetworkBase, nn.Module):
    def __init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer, out_channels, nonlin, num_filters):
        NetworkBase.__init__(self, device, network_type, loss, accuracy, lr, in_channels, optimizer, out_channels=out_channels, nonlin=nonlin, num_filters=num_filters)
        nn.Module.__init__(self)
        if self.num_filters == None:
            self.num_filters = 64

        self.conv_1_1 = self._conv_bn_layer(self.in_channels, self.num_filters, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_1_2 = self._conv_bn_layer(self.num_filters, self.num_filters, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.pool_1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_2_1 = self._conv_bn_layer(self.num_filters, self.num_filters * 2, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_2_2 = self._conv_bn_layer(self.num_filters * 2, self.num_filters * 2, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.pool_2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_3_1 = self._conv_bn_layer(self.num_filters * 2, self.num_filters * 4, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_3_2 = self._conv_bn_layer(self.num_filters * 4, self.num_filters * 4, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_3_3 = self._conv_bn_layer(self.num_filters * 4, self.num_filters * 4, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_3_4 = self._conv_bn_layer(self.num_filters * 4, self.num_filters * 4, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.pool_3 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_4_1 = self._conv_bn_layer(self.num_filters * 4, self.num_filters * 8, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_4_2 = self._conv_bn_layer(self.num_filters * 8, self.num_filters * 8, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_4_3 = self._conv_bn_layer(self.num_filters * 8, self.num_filters * 8, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_4_4 = self._conv_bn_layer(self.num_filters * 8, self.num_filters * 8, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.pool_4 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv_5_1 = self._conv_bn_layer(self.num_filters * 8, self.num_filters * 8, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_5_2 = self._conv_bn_layer(self.num_filters * 8, self.num_filters * 8, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_5_3 = self._conv_bn_layer(self.num_filters * 8, self.num_filters * 8, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.conv_5_4 = self._conv_bn_layer(self.num_filters * 8, self.num_filters * 8, filter_size=3, stride=1,
                                            nonlin_f=self.nonlin_f, padding=1)
        self.pool_5 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.fc_1 = nn.Linear(self.num_filters * 8, self.num_filters * 64)
        self.n_1 = self.nonlin_f()
        self.drop_1 = nn.Dropout2d()
        self.fc_2 = nn.Linear(self.num_filters * 64, self.num_filters * 64)
        self.n_2 = self.nonlin_f()
        self.drop_2 = nn.Dropout2d()
        self.out = nn.Linear(self.num_filters * 64, out_channels)

        # self.fc_1 = nn.Linear(num_filters * 8, num_filters * 8)
        # self.n_1 = self.nonlin_f()
        # self.drop_1 = nn.Dropout2d()
        # self.fc_2 = nn.Linear(num_filters * 8, num_filters * 4)
        # self.n_2 = self.nonlin_f()
        # self.drop_2 = nn.Dropout2d()
        # self.out = nn.Linear(num_filters * 4, out_channels)

    def forward(self, X):
        x = self.conv_1_1(X)
        x = self.conv_1_2(x)
        x = self.pool_1(x)

        x = self.conv_2_1(x)
        x = self.conv_2_2(x)
        x = self.pool_2(x)

        x = self.conv_3_1(x)
        x = self.conv_3_2(x)
        x = self.conv_3_3(x)
        x = self.conv_3_4(x)
        x = self.pool_3(x)

        x = self.conv_4_1(x)
        x = self.conv_4_2(x)
        x = self.conv_4_3(x)
        x = self.conv_4_4(x)
        x = self.pool_4(x)

        x = self.conv_5_1(x)
        x = self.conv_5_2(x)
        x = self.conv_5_3(x)
        x = self.conv_5_4(x)
        x = self.pool_5(x)

        x = x.view(x.size(0), -1)

        x = self.fc_1(x)
        x = self.n_1(x)
        x = self.drop_1(x)
        x = self.fc_2(x)
        x = self.n_2(x)
        x = self.drop_2(x)
        x = self.out(x)

        return x