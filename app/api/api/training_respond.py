# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import request, g, session


from . import Resource
from .. import schemas

from models.models import *
from db.mongo import MongoConnector

from .session_substitute import session_substitute

class TrainingRespond(Resource):

    def post(self):
        if g.args.get("callback_id") in session_substitute:
            return {'callback_id': g.args.get("callback_id"),
                    'current_status': session_substitute[g.args.get("callback_id")]['progress_state']}, 200, None
        else:
            return {'callback_id': g.args.get("callback_id"),
                    'current_status': 'No session with such callback was found'}, 200, None