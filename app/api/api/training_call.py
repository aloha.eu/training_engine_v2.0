# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from flask import g, session

from . import Resource
from celery.result import AsyncResult

from models.models import *
from db.mongo import MongoConnector

from optparse import OptionParser
from mongoengine.errors import ValidationError


from settings import settings
from celer.tasks import *
from celer.cc import celery
import requests
import threading
from multiprocessing import Process
from threading import Thread
import time

# from ..engine.main import run
from ..engine.utils.DataParser import DataParser
from ..engine.network.TrainRunner import TrainRunner


from .session_substitute import session_substitute

def pipeline_simulation(mongo):
    """
    pipeline simulation of previous steps
    :param mongo:
    :return:
    """

    mongo.db.user.drop()
    mongo.db.dataset.drop()
    mongo.db.project.drop()
    mongo.db.statuste.drop()
    mongo.db.algorithmconfiguration.drop()
    mongo.db.configurationfile.drop()

    usr = User()
    usr.email = "tst@mail.at"
    usr.password = "pwd"
    usr.save()

    #
    prj = Project()
    prj.creator = usr.get_id()
    prj.save()

    dsp = AlgorithmConfiguration()
    dsp.project = prj.get_id()
    dsp.onnx = "ConvNet"
    dsp.save()

    config = ConfigurationFile()

    config.project = prj.get_id()

    config.data_set_name = "MNIST"
    config.data_folder = None
    config.data_file = None

    config.class_names = None
    config.num_classes = 10

    #
    config.cross_val = 1
    config.image_dimensions = [28, 28, 1]
    config.task_type = 'classification'
    config.num_epochs = 10
    config.lr = 0.01
    config.lr_decay = 0.5
    config.ref_steps = 4
    config.ref_patience = 2
    config.batch_size = 16
    config.loss = 'softmax'
    config.accuracy = 'percent'
    config.optimizer = 'sgd'
    config.nonlin = 'relu'
    config.training_mode = 'full'
    config.num_filters = 16
    config.augment_dict = {'HorizontalFlip': None,
                           'VerticalFlip': None,
                           'Scale': None,
                           'Rotate': None,

                           'Normalize': False,

                           'AudioAugmentation': False,
                           'AddBGNoise': False}
    config.save()

    return prj.get_id(), dsp.get_id()


class TrainingCall(Resource):

    def post(self):

        def start_process(project_id, designpoint_id, callback_id, conn_config):
            # global session_substitute
            print('start_process ')

            # # REAL CASE SCENARIO ======== START #
            # dataset_task = create_dataset.apply_async(args=(project_id, designpoint_id, callback_id))
            # # REAL CASE SCENARIO ======== END #

 #           project_id, designpoint_id = pipeline_simulation(mongo)
            dataset_task = create_dataset.apply_async(args=(project_id, designpoint_id, callback_id, conn_config))


            result_state = AsyncResult(dataset_task.task_id, app=celery)
            result_state.get(on_message=self._on_raw_message, propagate=False)

        from flask import current_app
        with current_app.app_context():
            app = current_app._get_current_object()
        connection_config = app.config['MONGODB_SETTINGS']
        mongo = MongoConnector(app)


        if g.args.get("callback_id") in session_substitute:
            return {'callback_id': g.args.get("callback_id"), 'current_status': 'Task with given parametrs already exists'}, 200, None
        else:
            session_substitute[g.args.get("callback_id")] = {'project_id': g.args.get("project_id"),
                                                             'designpoint_id': g.args.get("designpoint_id"),
                                                             'progress_state': 'Job accepted'}

            thr = Thread(target=start_process, args=(g.args.get("project_id"), g.args.get("designpoint_id"), g.args.get("callback_id"), connection_config))
            thr.start()
            return {'callback_id': g.args.get("callback_id"), 'current_status': 'Job accepted'}, 200, None


    @classmethod
    def _on_raw_message(cls, body):
        result = body.get('result')
        if body.get('status') == 'SUCCESS ':
            # session_substitute[g.args.get("callback_id")]['progress_state'] = result.get('status')
            cls._send_requests(cls, result.get('callback_id'), result.get('status'))
        elif body.get('status') == 'PROGRESS':
            # print('on raw: changed')
            # print(session_substitute)
            # session_substitute[g.args.get("callback_id")]['progress_state'] = result.get('status')
            if result.get('status') == "FINISHED":
                cls._send_requests(cls, result.get('callback_id'), result.get('status'))
            else:
                cls._send_fake_request(cls, result.get('callback_id'), result.get('status'))
        elif body.get('status') == 'FAILED':
            
            cls._send_fake_request(cls, result.get('callback_id'), result.get('status'))
            #url='http://%s/api/dse_engine/failed?job_id=%s'%('dse_engine:5000',result.get('callback_id'))
            #r = requests.post(url,data={"module": "tra_en"})
            ### **** End of FAIL response
            #cls._send_fake_request(cls, 'None', 'Task failed')
    #

    def _send_requests(self, callback_id, task_state):
        session_substitute[callback_id]['progress_state'] = task_state
        host = 'dse_engine:5000'
        api = 'http://%s/api/dse_engine/callback?callback_id=%s'%(host, callback_id)
        r = requests.post(api)

        # print('request send')


    def _send_fake_request(self, callback_id, task_state):
        session_substitute[callback_id]['progress_state'] = task_state
        r = self._simulate_state_return(self, callback_id, task_state)
        print(r)

    def _simulate_state_return(self, callback_id, task_state):
        print("fake_return")
        return {'callback_id': callback_id,
                'current_status':  task_state}, 200, None
