#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 11/14/2018 12:12 PM $
# by : shepeleva $
# SVN  $
#

# --- imports -----------------------------------------------------------------


import os

from celery import Celery

REDIS_HOST = os.environ.get('REDIS_HOST', 'redis')
REDIS_PORT = os.environ.get('REDIS_PORT', '6379')
#if REDIS_PORT_6379_TCP_PORT is defined use that. This allows the user to set any of the two variables
REDIS_PORT = os.environ.get('REDIS_PORT_6379_TCP_PORT', REDIS_PORT)

BROKER_URL = 'redis://%s:%s/0' % (REDIS_HOST, REDIS_PORT)
BACKEND_URL = 'redis://%s:%s/0' % (REDIS_HOST, REDIS_PORT)


celery = Celery('celer',
                broker=BROKER_URL,
                backend=BACKEND_URL,
                include=['celer.tasks'])
