#!/usr/bin/env python
# -----------------------------------------------------------------------------
# Copyright (C) Software Competence Center Hagenberg GmbH (SCCH)
# All rights reserved.
# -----------------------------------------------------------------------------
# This document contains proprietary information belonging to SCCH.
# Passing on and copying of this document, use and communication of its
# contents is not permitted without prior written authorization.
# -----------------------------------------------------------------------------
# Created on : 11/14/2018 2:48 PM $ 
# by : shepeleva $ 
# SVN  $
#

# --- imports -----------------------------------------------------------------
from celer.cc import celery
from models.models import *
from api.engine.utils.DataParser import DataParser
from api.api.training_call import *
from api.engine.network.TrainRunner import TrainRunner
from mongoengine import *
from bson import ObjectId
import traceback


SHARED_DATA_FOLDER = "/opt/www/shared_data/"
from billiard import Process

def start_training(conf, alg, dtst, pipeline_dict):
    max_attemps= 10
    a=0
    status = "start"
    while (status=="NaN" or status =="start") and a<max_attemps:
      training_setup = TrainRunner(conf, alg, dtst, pipeline_dict)
      status = training_setup.start_training()
      print(status)
      if status=="NaN":
        conf.lr = conf.lr * conf.lr_decay
        a+=1
        print("Loss function returned NaN value.")
        print("Attempt {} of {}. New LR: {}".format(a+1, max_attemps, conf.lr))

@celery.task(bind=True)
def create_dataset(self, project_id, designpoint_id, callback_id, connection_config):
    try:
        connect(connection_config['db'], host=connection_config['host'], port=connection_config['port'])
        self.update_state(state='PROGRESS', meta={'status': 'DATASET CREATION',
                                                  'project_id': project_id,
                                                  'designpoint_id': designpoint_id,
                                                  'callback_id': callback_id})
        try:
            conf = ConfigurationFile.objects.get(project=project_id)
            
            
            try:
              alg = AlgorithmConfiguration.objects.get(id=ObjectId(designpoint_id))
            except AlgorithmConfiguration.DoesNotExist:
              self.update_state(state='FAILURE', meta={'status': "AlgorithmConfiguration does not exist",
                                                             'project_id': project_id,
                                                             'designpoint_id': designpoint_id,
                                                             'callback_id': callback_id})
               
            
            
            try:
              preprocessing_pipeline = Pipeline.objects.get(id=ObjectId(alg.pipeline))
            except:
              print("No pipeline associated to Algorithm. Using the project's pipeline")
              preprocessing_pipeline = Pipeline.objects.get(id=ObjectId(conf.pipeline))
            
            
            try:
              dtst = Dataset.objects(project=project_id).get(pipeline=str(alg.pipeline))
              print("dataset already exist")
              self.update_state(state='PROGRESS', meta={'status': "Dataset exists",
                                                        'project_id': project_id,
                                                        'designpoint_id': designpoint_id,
                                                        'callback_id': callback_id})
            except Dataset.DoesNotExist:
              print("dataset does not exist, creating one")
             
              print('start processing')
              data_parser = DataParser(conf, alg, preprocessing_pipeline)
              try:
                inst = data_parser.load_dataset()
                dtst = Dataset.objects(project=project_id).get(pipeline=str(alg.pipeline))
                self.update_state(state='PROGRESS', meta={'status': inst,
                                                            'project_id': project_id,
                                                            'designpoint_id': designpoint_id,
                                                            'callback_id': callback_id})
              except Exception as ex:
                print("Error creating Dataset")
                traceback.print_exc()
                
                self.update_state(state='FAILED', meta={'status': "Error creating Dataset",
                                                         'project_id': project_id,
                                                         'designpoint_id': designpoint_id,
                                                         'callback_id': callback_id
                                                          })
                
                return {'project_id': project_id, 'designpoint_id': designpoint_id,
                        'callback_id': callback_id, 'status': 'Process failed'}
                

            self.update_state(state='PROGRESS', meta={'status': 'TRAINING',
                                                      'project_id': project_id,
                                                      'designpoint_id': designpoint_id,
                                                      'callback_id': callback_id})


            p = Process(target=start_training, args=(conf, alg, dtst, preprocessing_pipeline))
            p.start()
            p.join()  # this blocks until the process terminates

            self.update_state(state='PROGRESS', meta={'status': 'FINISHED',
                                                      'project_id': project_id,
                                                      'designpoint_id': designpoint_id,
                                                      'callback_id': callback_id})
        except ConfigurationFile.DoesNotExist:
            traceback.print_exc()
            self.update_state(state='FAILED', meta={'status': "ConfigurationFile does not exist",
                                                     'project_id': project_id,
                                                     'designpoint_id': designpoint_id,
                                                     'callback_id': callback_id})
    except Exception as e:
        traceback.print_exc()
        self.update_state(state='FAILED', meta={ 'status': "FAIL: can't connect to database",
                                                 'project_id': project_id,
                                                 'designpoint_id': designpoint_id,
                                                 'callback_id': callback_id})

    return {'project_id': project_id, 'designpoint_id': designpoint_id,
            'callback_id': callback_id, 'status': 'Process completed'}
